import React, { useContext, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import { Route, Switch } from 'react-router-dom';
import Authentification from 'components/auth/Authentification';
import Home from 'components/Home';
import Chat from 'components/Chat';
import { FirebaseAuth } from 'provider/AuthProvider';
import PrivateRoute from 'routes/PrivateRoute';
import PublicRoute from 'routes/PublicRoute';
import Header from 'components/header/Header';
import Footer from 'components/footer/Footer';
import GenericNotFound from 'components/GenericNotFound';
import SpinnerWin from 'components/spinner/SpinnerWin';
import { collectionsMenu } from 'config/appConfig';

function App() {
  const { user, loading } = useContext(FirebaseAuth);

  return (
    <div className="container">
      <SpinnerWin loading={loading} />
      <Header />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/home" component={Home} />
        <PrivateRoute
          exact
          path="/chat"
          authenticated={user ? true : false}
          component={Chat}
        />

        {/* Collection menu */}
        {collectionsMenu.map((item) => {
          const submenu = collectionsMenu.filter(
            (e) => e.parent && e.parent === item.key && e.to
          );
          return (
            <PrivateRoute
              key={item.name}
              exact
              path={item.to}
              authenticated={user ? true : false}
              submenu={submenu}
              component={item.component}
            />
          );
        })}
        <PublicRoute
          exact
          path="/signin"
          authenticated={user ? true : false}
          component={Authentification}
        />
        <Route component={GenericNotFound} />
      </Switch>
      <Footer />
    </div>
  );
}

export default App;
