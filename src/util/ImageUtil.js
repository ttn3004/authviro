export function readImage(file) {
  return new Promise(async (resolve, reject) => {
    let img = new Image();
    img.src = window.URL.createObjectURL(file);
    img.onload = () => {
      const obj = { width: img.width, height: img.height };
      resolve(obj);
    };
  });
}
const regex = new RegExp('([a-zA-Z0-9s_\\.-:])+(.jpg|.png|.gif)$');
export function isImageFile(file) {
  return regex.test(file.name.toLowerCase());
}
