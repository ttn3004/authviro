export function maxKeyValue(arr) {
  let maxKey = 0;
  let maxVal = 0;
  arr.forEach((e) => {
    if (maxKey < e.x) {
      maxKey = e.x;
    }
    if (maxVal < e.y) {
      maxVal = e.y;
    }
  });
  return { x: maxKey, y: maxVal };
}
