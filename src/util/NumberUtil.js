export function random(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}
export function randomDouble(min, max) {
  const nb = Number(Math.random() * (max - min + 1) + min);
  return Number(nb.toFixed(2));
}
export function distanceArray3D(arrA, arrB) {
  const a = convertTo3DPoint(arrA);
  const b = convertTo3DPoint(arrB);
  return distance3D(a, b);
}
export function distance3D(a, b) {
  return Math.sqrt(
    Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2) + Math.pow(a.z - b.z, 2)
  );
}
export function convertTo3DPoint(arr) {
  return { x: arr[0], y: arr[1], z: arr[2] };
}
export function randomIntWithSum(nb, sum) {
  const arr = [];
  const part = Math.floor(sum / nb);
  // console.log('part', part, 'nb', nb);
  for (let i = 0; i < nb; i++) {
    let rd = random(2, part); //minimum column is 2 of 12 ,
    if (i === nb - 1) {
      const totalValArr = arr.reduce((pv, cv) => pv + cv, 0);
      rd = sum - totalValArr;
    }
    arr.push(rd);
  }
  return randomArrayShuffle(arr);
}
export function randomArrayShuffle(array) {
  var currentIndex = array.length,
    temporaryValue,
    randomIndex;
  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array;
}
