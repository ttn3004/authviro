import React from 'react';

import { random, randomIntWithSum, randomArrayShuffle } from 'util/NumberUtil';

export function generateGrid(row, col, rowClass, colClass) {
  let content = '';
  for (let i = 1; i <= row; i++) {
    content += `<div className="${rowClass}">`;
    for (let j = 1; j <= col; j++) {
      content += `<div className="${colClass}">`;
      content += '</div>';
    }
    content += '</div>';
  }

  return content;
}
export function generate(level, maxCol, nbCol, rowClass, colClass, arrGrid) {
  let jsxContent = [];
  for (let i = 0; i < level; i++) {
    const arr = randomIntWithSum(nbCol, maxCol); //ex: 3, 12 => we have 3 columns ans the sum is 12
    const maxCell = Math.max(...arr);
    //console.log('level', i, arr, maxCell);
    const valCol = arrGrid[i];
    const minObj = findMinAttr(valCol, 'height');
    const arrContent = generateRow(
      rowClass,
      colClass,
      arr,
      maxCell,
      valCol,
      minObj
    );
    jsxContent.push(arrContent);
  }
  // console.log('jsxContent', jsxContent);
  return jsxContent;
}
export function generateRow(
  rowClass,
  colClass,
  arrCol,
  maxCell,
  arrGridRow,
  minObj
) {
  //console.log('min Height', minObj);
  const jsxContent = (
    <div className={rowClass} key={random(1, Date.now())}>
      {arrCol.map((i, key) => {
        const objCol = arrGridRow[key];
        //console.log(objCol, i, key);
        const url = objCol.url;

        return (
          <div
            style={{
              height: minObj.height,
              border: '1px solid blue',
              backgroundImage: `url("${url}")`,
              backgroundPosition: 'center',
              backgroundRepeat: 'no-repeat',
              backgroundSize: 'cover',
            }}
            className={`${colClass}${i}`}
            key={random(1, Date.now())}
          >
            {/* {i} */}
          </div>
        );
      })}
    </div>
  );

  return jsxContent;
}
export function findMinAttr(arr, attr) {
  let minObj = null;
  let minVal = Number.MAX_SAFE_INTEGER ;
  for (let i = 0; i < arr.length; i++) {
    const obj = arr[i];
    if (obj[attr] && obj[attr] < minVal) {
      minVal = obj[attr];
      minObj = obj;
    }
  }
  return minObj;
}
export function findMaxAttr(arr, attr) {
  let maxObj = null;
  let maxVal = 0;
  for (let i = 0; i < arr.length; i++) {
    const obj = arr[i];
    if (obj[attr] && obj[attr]> maxVal) {
      maxVal = obj[attr];
      maxObj = obj;
    }
  }
  return maxObj;
}

export function getGridProps(items, NB_COL) {
  let nbRow = 1;
  let nbCol = NB_COL ? NB_COL : random(2, 6);
  const lg = items.length;

  let rest = null;
  let nbColFound = lg;
  const arrOfNbColFound = [];
  while (nbColFound > 2) {
    nbColFound--;
    rest = lg % nbColFound;
    if (rest === 0) arrOfNbColFound.push(nbColFound);
  }

  nbCol =
    arrOfNbColFound.length === 0 ? lg : randomArrayShuffle(arrOfNbColFound)[0];

  //console.log('res', nbCol, arrOfNbColFound);
  if (lg <= nbCol) return { nbRow, nbCol };
  nbRow = Math.floor(lg / nbCol);

  return { nbRow, nbCol };
}
export function getRandomGridItems(nbItem) {
  const arr = [];
  for (let i = 0; i < nbItem; i++) {
    const width = random(100, 800);
    const height = random(200, 600);
    const objCol = {
      uid: i,
      name: 'Slide number ' + i,
      description: 'Description for slide ' + i,
      height: height,
      width: width,
      url: getImageRandomSize(width, height),
    };
    arr.push(objCol);
  }
  return arr;
}
const getImageRandomSize = (width, height) => {
  return `https://placeholder.pics/svg/${width}x${height}`;
};
