import React from 'react';
import { CSSTransition } from 'react-transition-group';
const CSSTransitionComponent = ({ inProp, ...props }) => {
  return (
    <CSSTransition
      unmountOnExit
      in={inProp}
      timeout={{ appear: 0, enter: 0, exit: 300 }}
      classNames="roll"
      appear
    >
        <h2>Gator</h2>
    </CSSTransition>
  );
};
export default CSSTransitionComponent;
