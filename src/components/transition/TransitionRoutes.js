import React from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { useLocation } from 'react-router-dom';
import 'components/transition/csstransition.css';
const TransitionRoutes = ({ inProp, ...props }) => {
  const location = useLocation();
  return (
    <React.Fragment>
      <TransitionGroup>
        <CSSTransition
          key={location.key}
          classNames="fade"
          timeout={{
            enter: 500,
            exit: 500,
          }}
        >
          {props.children}
        </CSSTransition>
      </TransitionGroup>
    </React.Fragment>
  );
};
export default TransitionRoutes;
