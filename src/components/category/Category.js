import React, { useEffect } from 'react';
import {
  firebaseCollection as modelCollection,
  orderCollection,
  formAddCategory,
  formAddModelStyle,
  formAddModelBtnSubmit,
  formAddModelTitle,
  needConfirmToDelete,
} from 'config/categoryConfig.js';
import GenericPage from 'components/generic/GenericPage';
import { genericModel } from 'firebase/dbmethods';
const Category = ({ submenu, ...props }) => {
  return (
    <React.Fragment>
      <GenericPage
        modelCollection={modelCollection}
        orderCollection={orderCollection}
        submenu={submenu}
        dbModel={genericModel}
        title={formAddModelTitle}
        json={formAddCategory}
        addStyle={formAddModelStyle}
        btnsubmit={formAddModelBtnSubmit}
        needConfirmToDelete={needConfirmToDelete}
      />
    </React.Fragment>
  );
};

export default Category;
