import React, { useContext, useState } from 'react';
import { AsyncTypeahead } from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import { algoliaSearch } from 'algolia/algoliaIndex';
import { algolia } from 'config/appConfig';

const SEARCH_URI = 'https://api.github.com/search/users';
const TypeAheadSearch = (props) => {
  const [isLoading, setIsLoading] = useState(false);
  const [options, setOptions] = useState([]);
  const handleSearch = (query) => {
    setIsLoading(true);

    // with params
    algoliaSearch
      .search(query, {
        attributesToRetrieve: algolia.toRetrieve,
        hitsPerPage: algolia.hitsPerPage,
      })
      .then(({ hits }) => {
        console.log(hits);
        const options = hits.map((i) => ({
          id: i.objectID,
          value: i.value,
        }));

        setOptions(options);
        setIsLoading(false);
      })
      .catch((error) => {
        console.log('error on search algolia', error);
        setIsLoading(false);
      });
  };
  return (
    <form className="form-inline my-2 my-lg-0">
      <AsyncTypeahead
        id="async-example"
        isLoading={isLoading}
        labelKey={algolia.search.labelKey}
        minLength={algolia.search.minLength}
        onSearch={handleSearch}
        options={options}
        placeholder={algolia.search.placeholder}
        renderMenuItemChildren={(option, props) => (
          <React.Fragment>
            <span>{option.value}</span>
          </React.Fragment>
        )}
      />
    </form>
  );
};
export default TypeAheadSearch;
