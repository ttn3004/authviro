import React, { useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { FirebaseAuth } from 'provider/AuthProvider';
import { collectionsMenu } from 'config/appConfig';
import TypeAheadSearch from 'components/header/TypeAheadSearch';
const Header = (props) => {
  const { user, handleSignout } = useContext(FirebaseAuth);
  useEffect(() => {
    //console.log('Header', user);
  }, [user]);
  const signout = () => {
    return handleSignout();
  };
  return (
    <React.Fragment>
      <nav className="navbar navbar-light bg-light">
        <Link className="navbar-brand" to="/">
          Bootstrap
        </Link>
        <TypeAheadSearch />
        {/* menu for collection */}
        {collectionsMenu.map((item) => {
          if (item.key) {
            return (
              <Link className="nav-item nav-link" to={item.to} key={item.key}>
                {item.name}
              </Link>
            );
          }
        })}

        <Link className="nav-item nav-link" to="/chat">
          Chat
        </Link>
        {user ? (
          <button
            className="btn btn-outline-success my-2 my-sm-0"
            type="button"
            onClick={() => signout()}
          >
            Signout
          </button>
        ) : (
          ''
        )}
      </nav>
    </React.Fragment>
  );
};
export default Header;
