import React, { useContext, useEffect, useState, useRef } from 'react';
import { FirebaseAuth } from 'provider/AuthProvider';
import { chatMethods } from 'firebase/chatmethods';
import { db, storageRef } from 'firebase/firebaseIndex';
const Chat = (props) => {
  const { user, handleSignout } = useContext(FirebaseAuth);
  const [chats, setchats] = useState([]);
  const [content, setcontent] = useState('');
  const [errors, seterrors] = useState(null);
  const fileInput = useRef(null);
  useEffect(() => {
    const collection = db.collection('chats');

    /*     const unsubscribe = collection.get().then((snapshot) => {
      snapshot.forEach((doc) => {
        console.log(doc.data().content);
      });
    }); */
    const unsubscribe = collection
      .orderBy('timestamp', 'desc')
      .limit(10)
      .onSnapshot(
        (doc) => {
          const newArr = [];
          doc.forEach((element) => {
            newArr.push(element.data());
          });
          setchats(newArr);
        },
        (error) => {
          console.error(error);
        }
      );
    return () => unsubscribe();
  }, []);

  const handleChange = (e) => {
    setcontent(e.target.value);
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    const msg = {
      content: content,
      timestamp: Date.now(),
      email: user.email,
    };
    await db
      .collection('chats')
      .add(msg)
      .then((res) => {
        setcontent('');
        console.log('Document successfully written!');
      })
      .catch(function (error) {
        setcontent('');
        console.error('Error writing document: ', error);
      });
  };
  const handleUpload = async (e) => {
    e.preventDefault();
    const file = fileInput.current.files[0];
    console.log('file', file.name);
    storageRef
      .child(`images/${file.name}`)
      .put(file)
      .then((res) => {
        storageRef
          .child(`images/${file.name}`)
          .getDownloadURL()
          .then((url) => {
            console.log('img', url);
          });
      });
  };
  return (
    <React.Fragment>
      {/* chat */}
      <form className="form-inline" onSubmit={handleSubmit}>
        <div className="form-group  mr-sm-3">
          <input
            className="form-control"
            onChange={handleChange}
            placeholder="message"
            value={content}
          />
        </div>

        <button type="submit" className="btn btn-primary">
          Send
        </button>
      </form>

      <form className="form-inline mt-3" onSubmit={handleUpload}>
        <div className="custom-file form-control">
          <input
            ref={fileInput}
            type="file"
            className="custom-file-input form-control-file"
            id="validatedCustomFile"
            required
          />
          <label className="custom-file-label" htmlFor="validatedCustomFile">
            Choose file...
          </label>
        </div>

        <button type="submit" className="btn btn-warning ml-3">
          Upload
        </button>
      </form>

      <div
        className="mt-4"
        style={{
          border: '1px solid red',
          overflowY: 'scroll',
          minHeight: '100px',
          maxHeight: '500px',
        }}
      >
        {chats.map((m) => {
          const key = Math.floor(Math.random() * new Date().getTime());
          return (
            <p key={key}>
              <strong>{m.email}.</strong> {m.content}
            </p>
          );
        })}
      </div>
    </React.Fragment>
  );
};

export default Chat;
