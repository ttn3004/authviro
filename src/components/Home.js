import React, { useContext } from 'react';
import { withRouter, Link } from 'react-router-dom';
import GridGenerator from 'components/grid/GridGenerator';
import CarouselGenerator from 'components/carousel/CarouselGenerator';
import CarouselWithCustom from 'components/carousel/CarouselWithCustom';
import CarouselGridGenerator from 'components/carousel/CarouselGridGenerator';

const Home = (props) => {
  return (
    <React.Fragment>
      {/* <CarouselGenerator /> */}
      <CarouselWithCustom />
      <GridGenerator />
      <hr />
      <CarouselGridGenerator nbItem="3" />
    </React.Fragment>
  );
};
export default withRouter(Home);
