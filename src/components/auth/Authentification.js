// add useContext
import React, { useContext, useState, useEffect, useCallback } from 'react';
import { FirebaseAuth } from 'provider/AuthProvider';
import { Link } from 'react-router-dom';
import FormAuth from 'components/auth/FormAuth';
import ModalAuth from 'components/auth/ModalAuth';
import { needPhoneVerification } from 'config/appConfig';
import PhoneAuth from 'components/auth/PhoneAuth';
const Authentification = ({ ...props }) => {
  const {
    handleSignin,
    handleSignup,
    handleResetPwd,
    handleSigninWithGoogle,
    setfromLoc,
    setLoading,
    response,
    setResponse,
  } = useContext(FirebaseAuth);
  const [type, setType] = useState('signin');
  useEffect(() => {
    if (props.location && props.location.state && props.location.state.from) {
      setfromLoc(props.location.state.from);
    }
  }, []);
  useEffect(() => {
    if (response) setResponse([]);
  }, []);
  const createMarkup = useCallback((str) => {
    return { __html: str };
  }, []);
  const handleSubmit = useCallback(async (typeSubmit, email, password) => {
    try {
      if (typeSubmit === 'signin') {
        await handleSignin(email, password);
      } else if (typeSubmit === 'signup') {
        await handleSignup(email, password);
      } else if (typeSubmit === 'reset') {
        await handleResetPwd(email);
      }
    } catch (error) {
      console.error('error on ', type, error);
    }
    setLoading(false);
  }, []);

  const handleClose = useCallback(() => {
    setType('signin');
    setResponse([]);
  }, []);

  const handleOpenModalAuth = (type) => {
    setType(type);
    setResponse([]);
  };
  return (
    <React.Fragment>
      {needPhoneVerification && <PhoneAuth type={type} />}
      {!needPhoneVerification && (
        <React.Fragment>
          {type === 'signin' && (
            <React.Fragment>
              <FormAuth
                type="signin"
                response={response}
                handleSubmit={handleSubmit}
                createMarkup={createMarkup}
              />
              <hr />
              <button
                className="btn btn-warning mt-4"
                onClick={handleSigninWithGoogle}
              >
                Signin with google
              </button>
            </React.Fragment>
          )}

          <hr />
          <p>
            Don't have an account ?
            <a
              href="#/"
              className="mt-4"
              onClick={() => handleOpenModalAuth('signup')}
            >
              &nbsp; Sign up
            </a>
          </p>
          <p>
            Forget password and reset ?
            <a
              href="#/"
              className="mt-4"
              onClick={() => handleOpenModalAuth('reset')}
            >
              &nbsp;Reset password
            </a>
          </p>

          <ModalAuth
            show={
              type && (type === 'signup' || type === 'reset') ? true : false
            }
            type={type}
            response={response}
            handleSubmit={handleSubmit}
            handleClose={handleClose}
            createMarkup={createMarkup}
          />
        </React.Fragment>
      )}
    </React.Fragment>
  );
};
export default Authentification;
