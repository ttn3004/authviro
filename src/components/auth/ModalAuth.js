// add useContext
import React, { useContext, useEffect, useCallback } from 'react';
import FormAuth from 'components/auth/FormAuth';
import { Modal, Button } from 'react-bootstrap';

const ModalAuth = ({
  show,
  type,
  response,
  handleSubmit,
  handleClose,
  createMarkup,
  ...props
}) => {
  return (
    <Modal
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      show={show}
      onHide={() => console.log()}
    >
      <Modal.Header>
        <Modal.Title>
          {type === 'reset'
            ? 'Reset your password'
            : type === 'signup'
            ? 'Signup'
            : ''}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <FormAuth
          type={type}
          response={response}
          handleSubmit={handleSubmit}
          createMarkup={createMarkup}
        />
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
export default ModalAuth;
