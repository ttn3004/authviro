// add useContext
import React, { useState, useEffect, useContext } from 'react';
import firebase from 'firebase';
import { FirebaseAuth } from 'provider/AuthProvider';

const PhoneAuth = ({ type, ...props }) => {
  const {
    handleSendCode,
    handleVerifyCode,
    response,
    setResponse,
  } = useContext(FirebaseAuth);

  const [phone, setPhone] = useState('+16505550101');
  const [confirmResult, setConfirmResult] = useState(null);
  const [verificationCode, setVerificationCode] = useState('');
  const [changePhone, setChangePhone] = useState(false);

  const validatePhoneNumber = () => {
    const regexp = /^\+[0-9]?()[0-9](\s|\S)(\d[0-9]{8,16})$/;
    const res = regexp.test(phone);
    return res;
  };

  const changePhoneNumber = () => {
    setPhone('');
    setChangePhone(false);
    setConfirmResult(null);
    setVerificationCode('');
    setResponse([]);
  };


  /**Phone auth */
  const handleSubmitForm = async (e) => {
    e.preventDefault();
    if (confirmResult && phone) {
      if (verificationCode.length === 6) {
        try {
          const verificationId = confirmResult.verificationId;
          const credential = firebase.auth.PhoneAuthProvider.credential(
            verificationId,
            verificationCode
          );
          await firebase.auth().signInWithCredential(credential);
          //await handleVerifyCode(window.confirmationResult, verificationCode);
        } catch (error) {
          setResponse([{ type: 'error', msg: error.message }]);
        }
      } else {
        setResponse([
          { type: 'error', msg: 'Please enter a 6 digit OTP code.' },
        ]);
      }
    } else {
      const appVerifier = new firebase.auth.RecaptchaVerifier(
        'recaptcha-container'
      );
      if (validatePhoneNumber()) {
        try {
          const res = await handleSendCode(phone, appVerifier);
          setConfirmResult(res);
          setChangePhone(true);
        } catch (error) {
          setResponse([{ type: 'error', msg: error.message }]);
        }
      } else setResponse([{ type: 'error', msg: 'Phone number is error' }]);
    }
  };

  return (
    <React.Fragment>
      <form onSubmit={(e) => handleSubmitForm(e)}>
        <div className="form-group">
          <label htmlFor="exampleInputPhone">Phone</label>
          <div className="input-group mb-3">
            <input
              type="text"
              className="form-control"
              id="exampleInputPhone"
              onChange={(e) => setPhone(e.target.value)}
              name="phone"
              placeholder="phone"
              value={phone}
              readOnly={changePhone}
            />
            <div className="input-group-append">
              <button
                className="btn btn-outline-secondary"
                type="button"
                id="button-addon2"
                onClick={() => changePhoneNumber()}
              >
                Modify
              </button>
            </div>
          </div>
        </div>
        {confirmResult && (
          <div className="form-group">
            <label htmlFor="verificationPhoneCode">Verification code</label>
            <input
              type="text"
              className="form-control"
              id="verificationPhoneCode"
              onChange={(e) => setVerificationCode(e.target.value)}
              name="verificationPhoneCode"
              placeholder="Verification code"
              value={verificationCode}
            />
          </div>
        )}
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>

      {response.length > 0 ? (
        <React.Fragment>
          <hr />
          {response.map((res) => {
            return (
              <div
                key={res.msg}
                className={`alert ${
                  res.type === 'info'
                    ? 'alert-success'
                    : res.type === 'error'
                    ? 'alert-danger'
                    : ''
                }`}
                role="alert"
              >
                {res.msg}
              </div>
            );
          })}
        </React.Fragment>
      ) : null}

      {!confirmResult && <div id="recaptcha-container" className="my-4"></div>}
    </React.Fragment>
  );
};
export default PhoneAuth;
