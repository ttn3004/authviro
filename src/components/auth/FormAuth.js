// add useContext
import React, { useState } from 'react';
const FormAuth = ({ type, response, handleSubmit, createMarkup, ...props }) => {
  const [inputs, setinputs] = useState({
    email: 'pinkin@gmail.com',
    password: '123456',
  });
  const handleChange = (e) => {
    const { name, value } = e.target;
    //console.log(inputs);
    setinputs((prev) => ({ ...prev, [name]: value }));
  };
  const handleSubmitForm = (e) => {
    e.preventDefault();
    handleSubmit(type, inputs.email, inputs.password);
    setinputs({
      email: '',
      password: '',
      phone: '',
    });
  };

  return (
    <React.Fragment>
      <form onSubmit={(e) => handleSubmitForm(e)}>
        <div className="form-group">
          <label htmlFor="exampleInputEmail1">Email address</label>
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text" id="basic-addon1">
                &#128231;
              </span>
            </div>
            <input
              type="email"
              className="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              onChange={(e) => handleChange(e)}
              name="email"
              placeholder="email"
              value={inputs.email}
            />
          </div>

          <small id="emailHelp" className="form-text text-muted">
            We'll never share your email with anyone else.
          </small>
        </div>
        {(type === 'signin' || type === 'signup') && (
          <div className="form-group">
            <label htmlFor="pwdEmail">Password</label>
            <div className="input-group mb-3">
              <div className="input-group-prepend">
                <span className="input-group-text" id="basic-addon1">
                  🔑
                </span>
              </div>
              <input
                type="password"
                className="form-control"
                id="pwdEmail"
                onChange={(e) => handleChange(e)}
                name="password"
                placeholder="password"
                value={inputs.password}
              />
            </div>

            <small id="emailHelp" className="form-text text-muted">
              We'll never share your email with anyone else.
            </small>
          </div>
        )}

        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>

      {response.length > 0 ? (
        <React.Fragment>
          <hr />
          {response.map((res) => {
            return (
              <div
                key={res.msg}
                className={`alert ${
                  res.type === 'info'
                    ? 'alert-success'
                    : res.type === 'error'
                    ? 'alert-danger'
                    : ''
                }`}
                role="alert"
              >
                {res.msg}
              </div>
            );
          })}
        </React.Fragment>
      ) : null}
    </React.Fragment>
  );
};
export default FormAuth;
