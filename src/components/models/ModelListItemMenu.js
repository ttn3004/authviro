import React, { useContext } from 'react';
import { dbModel } from 'firebase/dbmethods';

import { GenericCtx } from 'provider/GenericDbProvider';

const ModelListItemMenu = ({ doc }) => {
  const { handleResetPaging } = useContext(GenericCtx);
  const handleDelete = async () => {
    await dbModel.delete(doc);
    handleResetPaging();
  };
  return (
    <div className="card-footer text-muted">
      <button type="button" className="btn btn-danger" onClick={handleDelete}>
        Delete
      </button>
    </div>
  );
};
export default ModelListItemMenu;
