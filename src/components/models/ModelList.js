import React, { useContext } from 'react';
import { GenericCtx } from 'provider/GenericDbProvider';
import ModelListItem from 'components/models/ModelListItem';
const ModelList = (props) => {
  const { items, filter } = useContext(GenericCtx);
  return (
    <React.Fragment>
      {items && filter && (
        <div className="row row-cols-1 row-cols-md-3">
          {items.map((doc) => {
            return (
              <div className="col mb-4" key={doc.name}>
                <ModelListItem doc={doc} />
              </div>
            );
          })}
        </div>
      )}
    </React.Fragment>
  );
};
export default ModelList;
