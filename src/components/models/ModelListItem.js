import React, { useContext } from 'react';
import ModelListItemMenu from 'components/models/ModelListItemMenu';
const ModelListItem = ({ doc }) => {
  return (
    <div className="card">
      <svg
        className="bd-placeholder-img card-img-top"
        width="100%"
        height="200"
        xmlns="http://www.w3.org/2000/svg"
        preserveAspectRatio="xMidYMid slice"
        focusable="false"
        role="img"
        aria-label="Placeholder: Image cap"
      >
        <title>Placeholder</title>
        <rect width="100%" height="100%" fill="#868e96"></rect>
        <text x="50%" y="50%" fill="#dee2e6" dy=".3em">
          Image cap
        </text>
      </svg>

      <div className="card-body">
        <h5 className="card-title">{doc.name}</h5>
        <p className="card-text">
          This is a longer card with supporting text below as a natural lead-in
          to additional content. This content is a little bit longer.
        </p>
        <p className="card-text">
          <small className="text-muted">Last updated 3 mins ago</small>
        </p>
      </div>
      <ModelListItemMenu doc={doc} />
    </div>
  );
};
export default ModelListItem;
