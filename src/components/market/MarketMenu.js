import React, { useContext, useEffect, useState, useRef } from 'react';
import { FirebaseAuth } from 'provider/AuthProvider';
import { db } from 'firebase/firebaseIndex';
import { withRouter, Link } from 'react-router-dom';
const MarketMenu = (props) => {
  return (
    <ul className="nav justify-content-center">
      <li className="nav-item" key="1">
        <Link  to="/market/add" className="nav-link active" href="#/">
          Active
        </Link>
      </li>
      <li className="nav-item"  key="2">
        <Link  to="/market/add" className="nav-link" href="#/">
          Link
        </Link>
      </li>
      <li className="nav-item"  key="3">
        <Link  to="/market/add" className="nav-link" href="#/">
          Link
        </Link>
      </li>
      <li className="nav-item"  key="4">
        <Link
          className="nav-link"
          to="/market/add"
          tabIndex="-1"
          aria-disabled="true"
        >
          Add Model
        </Link>
      </li>
    </ul>
  );
};
export default MarketMenu;
