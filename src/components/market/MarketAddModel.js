import React, { useContext, useCallback, useState, useRef } from 'react';
import { dbModel } from 'firebase/dbmethods';
import {
  formAddModel,
  formAddModelStyle,
  formAddModelBtnSubmit,
  formAddModelTitle,
  firebaseCollection,
} from 'config/modelConfig';

import FormFactory from 'components/form/FormFactory';

const MarketAddModel = (props) => {
  const handleCallback = useCallback((res) => {
    console.log();
  }, []);
  return (
    <React.Fragment>
      <FormFactory
        title={formAddModelTitle}
        json={formAddModel}
        style={formAddModelStyle}
        btnsubmit={formAddModelBtnSubmit}
        firebaseCollection={firebaseCollection}
        dbModel={dbModel}
        handleCallback={handleCallback}
      />
    </React.Fragment>
  );
};
export default MarketAddModel;
