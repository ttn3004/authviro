import React, { useEffect } from 'react';
import {
  firebaseCollection as modelCollection,
  filterCollection,
  orderCollection,
  paging,
  formAddModel,
  formAddModelStyle,
  formAddModelBtnSubmit,
  formAddModelTitle,
  needConfirmToDelete,
} from 'config/modelConfig.js';
import { dbModel } from 'firebase/dbmethods';
import GenericPage from 'components/generic/GenericPage';
const Market = ({ submenu, ...props }) => {
  useEffect(() => {
    //console.log('Market useEffect', props);
  }, []);
  return (
    <React.Fragment>
      <GenericPage
        modelCollection={modelCollection}
        filterCollection={filterCollection}
        orderCollection={orderCollection}
        submenu={submenu}
        dbModel={dbModel}
        paging={paging}
        nbItemsPerPage={paging ? paging.itemsPerPage : null}
        json={formAddModel}
        title={formAddModelTitle}
        addStyle={formAddModelStyle}
        btnsubmit={formAddModelBtnSubmit}
        needConfirmToDelete={needConfirmToDelete}
      />
    </React.Fragment>
  );
};

export default Market;
