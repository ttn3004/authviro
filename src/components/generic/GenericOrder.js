import React, { useContext, useEffect, useState, useRef } from 'react';
import { BsChevronUp, BsChevronDown } from 'react-icons/bs';
import { GenericCtx } from 'provider/GenericDbProvider';

const GenericOrder = (props) => {
  const { orderFilter, setOrderFilter, handleResetPaging } = useContext(
    GenericCtx
  );

  const handleOrderField = (field, operator) => {
    //console.log('handlerOrderField', field, operator);
    const newArr = orderFilter;
    for (let i = 0; i < newArr.length; i++) {
      const obj = newArr[i];
      if (obj.field === field) {
        if (obj.operator === 'asc') {
          obj.operator = 'desc';
        } else {
          obj.operator = 'asc';
        }
        break;
      }
    }

    setOrderFilter(newArr);
    handleResetPaging();
  };
  return (
    <React.Fragment>
      {orderFilter && (
        <div className="btn-group" role="group" aria-label="order group">
          {orderFilter.map((obj) => {
            return (
              <button
                type="button"
                className="btn btn-secondary mr-sm-2"
                key={obj.field}
                onClick={() => handleOrderField(obj.field, obj.operator)}
              >
                {obj.label}
                {obj.operator === 'asc' ? (
                  <BsChevronUp className="ml-sm-2" />
                ) : obj.operator === 'desc' ? (
                  <BsChevronDown className="ml-sm-2" />
                ) : (
                  ''
                )}
              </button>
            );
          })}
        </div>
      )}
    </React.Fragment>
  );
};
export default GenericOrder;
