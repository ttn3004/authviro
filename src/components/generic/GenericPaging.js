import React, { useContext, useEffect, useState, useRef } from 'react';
import { GenericCtx } from 'provider/GenericDbProvider';
import FormFilter from 'components/generic/filter/FormFilter';

const GenericPaging = (props) => {
  const {
    currentPage,
    setCurrentPage,
    endOfPaging,
    setQueryChange,
  } = useContext(GenericCtx);
  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () =>
      window.removeEventListener(
        'scroll',
        handleScroll,
        document.documentElement.offsetHeight
      );
  }, []);
  function handleScroll() {
    if (endOfPaging) return;
    /*    console.log(
      'handleScroll',
      endOfPaging,
      window.innerHeight,
      document.documentElement.scrollTop,
      document.documentElement.offsetHeight
    ); */
    if (
      window.innerHeight + document.documentElement.scrollTop <
      document.documentElement.offsetHeight - 5
    )
      return;
    //console.log('Fetch more list items!');
    handleNext();
  }

  const handleNext = () => {
   // console.log('handleNext', endOfPaging);
    if (!endOfPaging) {
      setCurrentPage(currentPage + 1);
      setQueryChange('filter_' + Date.now());
    }
  };

  return <React.Fragment>{props.children}</React.Fragment>;
};
export default GenericPaging;
