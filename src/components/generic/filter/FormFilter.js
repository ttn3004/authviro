import React, { useEffect, useMemo, useState } from 'react';

import { useForm } from 'react-hook-form';
const FormFilter = ({ filter, handleFilter, resetForm, ...props }) => {
  const { register, handleSubmit, reset } = useForm();

  const displayForm = filter && filter.length > 0;
  const onSubmit = async (data, e) => {
    e.preventDefault();
    // console.log('onSubmit', data);
    // console.log('submit', data, filter);
    const newFilter = [];
    filter.forEach((element) => {
      if (data[element.key] && data[element.key] !== element.placeholder) {
        const valueForm = data[element.key];
        const newElement = element;
        newElement.value = valueForm;
        newFilter.push(newElement);
      } else {
        element.value = null;
        newFilter.push(element);
      }
    });
    //console.log('newFilter', newFilter);
    handleFilter(newFilter);
  };
  const handleFormReset = () => {
    reset();
    // handleFilter(filter);
    resetForm();
  };

  //filter with format {name:[]  | text , type : text}
  const formContent = filter ? (
    <React.Fragment>
      {filter.map((obj) => {
        let contentEl = '';
        switch (obj.type) {
          case 'text':
            contentEl = (
              <input
                key={obj.key}
                type={obj.type}
                className="form-control mb-2 mr-sm-2"
                name={obj.key}
                placeholder={obj.placeholder}
                ref={register}
              />
            );
            break;
          case 'select':
            contentEl = (
              <select
                key={obj.key}
                className="form-control mb-2 mr-sm-2"
                name={obj.key}
                ref={register}
                multiple={obj.multiple}
              >
                <option hidden>{obj.placeholder}</option>
                {obj.optionValues.map((e) => {
                  return (
                    <option key={e.uid} value={e.value}>
                      {e.value}
                    </option>
                  );
                })}
              </select>
            );
            break;
          case 'range':
            break;
          default:
            break;
        }
        return contentEl;
      })}
    </React.Fragment>
  ) : (
    ''
  );

  return (
    <React.Fragment>
      {displayForm && (
        <form className="form-inline" onSubmit={handleSubmit(onSubmit)}>
          {formContent}
          <button type="submit" className="btn btn-primary mb-2  mr-4">
            submit
          </button>
          <button
            onClick={handleFormReset}
            type="submit"
            className=" btn btn-outline-dark mb-2"
          >
            reset
          </button>
        </form>
      )}
    </React.Fragment>
  );
};

export default FormFilter;
