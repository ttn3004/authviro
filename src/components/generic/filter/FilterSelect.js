import React from 'react';

const FilterSelect = ({
  name,
  placeholder,
  optionvalues,
  multiple,
  ...props
}) => {
  return (
    <React.Fragment>
      <select
        multiple={multiple}
        id={name}
        className={`${controlClass} ${validClass}`}
        name={id}
        ref={register}
        readOnly={readOnly}
      >
        <option hidden>{placeholder}</option>
        {optionvalues && (
          <React.Fragment>
            {optionvalues.map((item) => {
              return <option key={item.id}>{item.name}</option>;
            })}
          </React.Fragment>
        )}
      </select>
    </React.Fragment>
  );
};
export default FilterSelect;
