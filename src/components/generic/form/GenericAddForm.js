import React, { useCallback } from 'react';
import FormFactory from 'components/form/FormFactory';
import { Modal, Button } from 'react-bootstrap';
import { genericModel } from 'firebase/dbmethods';
const GenericAddForm = ({
  title,
  json,
  style,
  btnsubmit,
  firebaseCollection,
  dbModel,
  show,
  handleClose,
  ...props
}) => {
  const handleCallback = useCallback((res) => {
    handleClose();
  }, []);
  return (
    <Modal
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      show={show}
    >
      <Modal.Header>
        <Modal.Title>{title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <FormFactory
          title={title}
          json={json}
          style={style}
          btnsubmit={btnsubmit}
          firebaseCollection={firebaseCollection}
          dbModel={genericModel}
          handleCallback={(res) => handleCallback(res)}
        />
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
export default GenericAddForm;
