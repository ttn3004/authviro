import React, { useCallback } from 'react';
import FormFactory from 'components/form/FormFactory';
import { Modal, Button } from 'react-bootstrap';

const GenericUpdateForm = ({
  title,
  json,
  style,
  btnsubmit,
  firebaseCollection,
  dbModel,
  show,
  handleClose,
  doc,
  ...props
}) => {
  //const callback = (res) => {};
  const handleCallback = useCallback((res) => {
    handleClose();
  }, []);
 // console.log('GenericUpdateForm', doc);
  return (
    <Modal
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      show={show}
      onHide={() => console.log()}
    >
      <Modal.Header>
        <Modal.Title>{title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <FormFactory
          title={title}
          json={json}
          style={style}
          btnsubmit={btnsubmit}
          firebaseCollection={firebaseCollection}
          dbModel={dbModel}
          handleCallback={(res) => handleCallback(res)}
          doc={doc}
        />
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
export default GenericUpdateForm;
