import React, { useContext, useEffect, useState, useRef } from 'react';
import { GenericCtx } from 'provider/GenericDbProvider';
import FormFilter from 'components/generic/filter/FormFilter';

const GenericFilter = (props) => {
  const {
    filter,
    setFilter,
    handleResetPaging,
    setQueryChange,
    handleResetQuery,
  } = useContext(GenericCtx);

  const handleFilter = (newFilter) => {
    handleResetPaging();
    setFilter(newFilter);
    setQueryChange('filter_' + Date.now());
  };

  const resetForm = () => {
    handleResetQuery();
  };
  return (
    <React.Fragment>
      {filter && (
        <FormFilter
          filter={filter}
          handleFilter={handleFilter}
          resetForm={resetForm}
        />
      )}
    </React.Fragment>
  );
};
export default GenericFilter;
