import React, { useContext } from 'react';
import GenericListItemMenu from 'components/generic/GenericListItemMenu';
const GenericListItem = ({
  doc,
  style,
  dbModel,
  collectionName,
  json,
  addStyle,
  btnsubmit,
  needConfirmToDelete,
  ...props
}) => {
  return (
    <div className={style.class}>
      <svg
        className={style.image.class}
        width={style.image.width}
        height={style.image.height}
        xmlns="http://www.w3.org/2000/svg"
        preserveAspectRatio="xMidYMid slice"
        focusable="false"
        role="img"
        aria-label={style.image.title}
      >
        <title>Placeholder</title>
        <rect width="100%" height="100%" fill="#868e96"></rect>
        <text x="50%" y="50%" fill="#dee2e6" dy=".3em">
          Image cap
        </text>
      </svg>

      <div className={style.body.class}>
        <h5 className={style.body.title}>{doc.value}</h5>
        <p className={style.body.text}>
          This is a longer card with supporting text below as a natural lead-in
          to additional content. This content is a little bit longer.
        </p>
        <p className={style.body.text}>
          <small className={style.body.small}>Last updated 3 mins ago</small>
        </p>
      </div>
      <GenericListItemMenu
        doc={doc}
        dbModel={dbModel}
        collectionName={collectionName}
        json={json}
        addStyle={addStyle}
        btnsubmit={btnsubmit}
        needConfirmToDelete={needConfirmToDelete}
      />
    </div>
  );
};
export default GenericListItem;
