import React, { useContext, useEffect, useState } from 'react';
import GenericList from 'components/generic/GenericList';
import GenericMenu from 'components/generic/menu/GenericMenu';
import GenericFilter from 'components/generic/GenericFilter';
import GenericOrder from 'components/generic/GenericOrder';

import GenericPaging from 'components/generic/GenericPaging';
import { pageStyle } from 'config/genericConfig';
import GenericDbProvider from 'provider/GenericDbProvider';

const GenericPage = ({
  modelCollection,
  filterCollection,
  orderCollection,
  paging,
  nbItemsPerPage,
  style,
  submenu,
  dbModel,
  title,
  json,
  addStyle,
  btnsubmit,
  needConfirmToDelete,
  ...props
}) => {
  useEffect(() => {
    //console.log('GenericPage useEffect', dbModel);
  }, []);
  return (
    <React.Fragment>
      <GenericDbProvider
        collectionName={modelCollection}
        filterCollection={filterCollection}
        orderCollection={orderCollection}
        paging={paging}
        nbItemsPerPage={nbItemsPerPage}
      >
        {submenu && (
          <GenericMenu
            menu={submenu}
            style={style ? style : pageStyle}
            title={title}
            json={json}
            addStyle={addStyle}
            btnsubmit={btnsubmit}
            firebaseCollection={modelCollection}
            dbModel={dbModel}
          />
        )}

        {filterCollection && (
          <GenericFilter style={style ? style : pageStyle} />
        )}
        {orderCollection && <GenericOrder style={style ? style : pageStyle} />}
        {paging && (
          <GenericPaging nbItemsPerPage={nbItemsPerPage}>
            <GenericList
              style={style ? style : pageStyle}
              dbModel={dbModel}
              collectionName={modelCollection}
              json={json}
              addStyle={addStyle}
              btnsubmit={btnsubmit}
              needConfirmToDelete={needConfirmToDelete}
            />
          </GenericPaging>
        )}
        {!paging && (
          <GenericList
            style={style ? style : pageStyle}
            dbModel={dbModel}
            collectionName={modelCollection}
            json={json}
            addStyle={addStyle}
            btnsubmit={btnsubmit}
            needConfirmToDelete={needConfirmToDelete}
          />
        )}
      </GenericDbProvider>
    </React.Fragment>
  );
};

export default GenericPage;
