import React, { useState, useCallback } from 'react';
import GenericUpdateForm from 'components/generic/form/GenericUpdateForm';
import ConfirmDialog from 'components/form/ConfirmDialog';
const GenericListItemMenu = ({
  doc,
  dbModel,
  collectionName,
  json,
  addStyle,
  btnsubmit,
  needConfirmToDelete,
  ...props
}) => {
  
  const [show, setShow] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const handleClose = useCallback(() => {
    setShow(false);
    setShowConfirm(false);
  }, []);

  const handleConfirmDelete = () => {
    if (needConfirmToDelete) {
      setShowConfirm(true);
    } else {
      handleDelete(true);
    }
  };

  const handleDelete = useCallback(async (confirm) => {
    if (confirm) {
      await dbModel.delete(doc, collectionName, json);
      console.log('handleDelete', doc);
      handleClose();
    }
  }, []);
  const handleOpenUpdateModal = async () => {
    setShow(true);
  };

  return (
    <React.Fragment>
      <div className="card-footer text-muted">
        <button
          type="button"
          className="btn btn-danger mr-4"
          onClick={handleConfirmDelete}
        >
          Delete
        </button>

        <button
          type="button"
          className="btn btn-warning"
          onClick={handleOpenUpdateModal}
        >
          Update
        </button>
      </div>
      <GenericUpdateForm
        show={show}
        title="update form"
        json={json}
        style={addStyle}
        btnsubmit={btnsubmit}
        firebaseCollection={collectionName}
        dbModel={dbModel}
        handleClose={handleClose}
        doc={doc}
      />
      {needConfirmToDelete && (
        <ConfirmDialog
          show={showConfirm}
          title={`Document id ${doc.uid} and their files will be deleted`}
          handleConfirm={handleDelete}
          handleClose={handleClose}
        />
      )}
    </React.Fragment>
  );
};
export default GenericListItemMenu;
