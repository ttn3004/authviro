import React, { useContext } from 'react';
import { GenericCtx } from 'provider/GenericDbProvider';
import GenericListItem from 'components/generic/GenericListItem';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import 'components/transition/csstransition.css';

const GenericList = ({
  style,
  dbModel,
  collectionName,
  json,
  addStyle,
  btnsubmit,
  needConfirmToDelete,
  ...props
}) => {
  const { items } = useContext(GenericCtx);
  return (
    <React.Fragment>
      {items && (
        <TransitionGroup className={style.list.class}>
          {items.map((doc) => {
            return (
              <CSSTransition
                key={doc.uid}
                timeout={500}
                classNames="generic-item"
              >
                <div className={style.listItem.class}>
                  <GenericListItem
                    doc={doc}
                    style={style.listItem.card}
                    dbModel={dbModel}
                    collectionName={collectionName}
                    json={json}
                    addStyle={addStyle}
                    btnsubmit={btnsubmit}
                    needConfirmToDelete={needConfirmToDelete}
                  />
                </div>
              </CSSTransition>
            );
          })}
        </TransitionGroup>
      )}
    </React.Fragment>
  );
};
export default GenericList;
