import React, { useContext } from 'react';
import { GenericCtx } from 'provider/GenericDbProvider';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import 'components/transition/csstransition.css';
import FormFactory from 'components/form/FormFactory';

const GenericOne = ({
  dbModel,
  title,
  firebaseCollection,
  json,
  addStyle,
  btnsubmit,
  doc,
  idGen,
  needEnableToChange,
  handleCallback,
  handleEnableChange,
  ...props
}) => {
  return (
    <React.Fragment>
      <FormFactory
        title={title}
        json={json}
        style={addStyle}
        btnsubmit={btnsubmit}
        firebaseCollection={firebaseCollection}
        dbModel={dbModel}
        handleCallback={handleCallback}
        doc={doc}
        idGen={idGen}
        needEnableToChange={needEnableToChange}
        handleEnableChange={handleEnableChange}
      />
    </React.Fragment>
  );
};
export default GenericOne;
