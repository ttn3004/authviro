import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import GenericMenuItem from 'components/generic/menu/GenericMenuItem';
const GenericMenu = ({
  menu,
  style,
  title,
  json,
  addStyle,
  btnsubmit,
  firebaseCollection,
  dbModel,
  ...props
}) => {
  /*   console.log(
    'GenericMenu',
    title,
    json,
    addStyle,
    btnsubmit,
    firebaseCollection
  ); */
  return (
    <React.Fragment>
      {menu && (
        <ul className={style.menu.nav}>
          {menu.map((item) => {
            if (item.component) {
              return (
                <li className={style.menu.item} key={item.name}>
                  <Link to={item.to} className={style.menu.link} href="#/">
                    {item.name}
                  </Link>
                </li>
              );
            } else {
              return (
                <li className={style.menu.item} key={item.name}>
                  <GenericMenuItem
                    name={item.name}
                    type={item.type}
                    title={item.name}
                    json={json}
                    addStyle={addStyle}
                    btnsubmit={btnsubmit}
                    firebaseCollection={firebaseCollection}
                    dbModel={dbModel}
                  />
                </li>
              );
            }
          })}
        </ul>
      )}
    </React.Fragment>
  );
};
export default GenericMenu;
