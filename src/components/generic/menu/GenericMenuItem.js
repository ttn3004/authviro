import React, { useState } from 'react';
import GenericAddForm from 'components/generic/form/GenericAddForm';
import GenericUpdateForm from 'components/generic/form/GenericUpdateForm';
const GenericMenuItem = ({
  name,
  type,
  menu,
  style,
  title,
  json,
  addStyle,
  btnsubmit,
  firebaseCollection,
  dbModel,
  ...props
}) => {
  /*  console.log(
    'GenericMenuItem',
    title,
    json,
    addStyle,
    btnsubmit,
    firebaseCollection
  ); */
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShowMenuItem = () => {
    setShow(true);
  };

  return (
    <React.Fragment>
      <a
        className="nav-link"
        id={name}
        data-toggle="pill"
        href="#/"
        role="button"
        onClick={() => handleShowMenuItem()}
      >
        {name}
      </a>
      {type === 'add' && (
        <GenericAddForm
          show={show}
          title={name}
          json={json}
          style={addStyle}
          btnsubmit={btnsubmit}
          firebaseCollection={firebaseCollection}
          dbModel={dbModel}
          handleClose={handleClose}
        />
      )}
      {type === 'update' && (
        <GenericUpdateForm
          show={show}
          title={name}
          json={json}
          style={addStyle}
          btnsubmit={btnsubmit}
          firebaseCollection={firebaseCollection}
          dbModel={dbModel}
          handleClose={handleClose}
        />
      )}
    </React.Fragment>
  );
};
export default GenericMenuItem;
