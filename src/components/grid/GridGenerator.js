import React, { useMemo, useEffect } from 'react';
import { generate, getGridProps, getRandomGridItems } from 'util/GridUtil';
import { CSSTransition } from 'react-transition-group';
import 'components/transition/csstransition.css';

const GridGenerator = ({ items, ...props }) => {
  //const gridLevel = 3;
  const NB_COL = 3;
  const ROW_CLASS = 'row';
  const COL_CLASS = 'col-';
  const MAX_COLUMN = 12; //by row for bootstrap

  const initalGrid = useMemo(() => genRow(), []);
  useEffect(() => {
    //console.log('GridGenerator');
  }, []);
  function genRow() {
    const itemsGrid = items ? items : getRandomGridItems(6);
    //console.log('genRow', items);
    const { nbRow, nbCol } = getGridProps(itemsGrid, null);
    // console.log('getGridProps', nbRow, nbCol);

    const arrGrid = Array.from(Array(nbRow), () => new Array(nbCol));
    let indexItem = 0;
    for (let i = 0; i < nbRow; i++) {
      for (let j = 0; j < nbCol; j++) {
        if (indexItem < itemsGrid.length) arrGrid[i][j] = itemsGrid[indexItem]; //precaution with if loop
        indexItem++;
      }
    }
    //console.log(arrGrid);
    const contentGrid = generate(
      nbRow,
      MAX_COLUMN,
      nbCol,
      ROW_CLASS,
      COL_CLASS,
      arrGrid
    );
    return <div>{contentGrid}</div>;
  }
  let content = <React.Fragment> {initalGrid}</React.Fragment>;
  return (
    <React.Fragment>
      <CSSTransition
        in={content ? true : false}
        timeout={500}
        classNames="grid-generator"
      >
        {content}
      </CSSTransition>
    </React.Fragment>
  );
};
export default GridGenerator;
