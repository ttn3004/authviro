import React, { useContext, useEffect } from 'react';
import { Modal, Button } from 'react-bootstrap';
const GlobalSpinner = ({ loading, ...props }) => {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      show={loading}
    >
      <Modal.Body></Modal.Body>
    </Modal>
  );
};
export default GlobalSpinner;
