import React from 'react';
import 'components/spinner/win.css';
import { CSSTransition } from 'react-transition-group';

const SpinnerWin = ({ loading, ...props }) => {
  /* https://icons8.com/cssload/ */
  return (
    <CSSTransition in={loading} timeout={200} classNames="spinner" unmountOnExit>
      <div className="global-spinner-overlay">
        <div className="windows8">
          <div className="wBall" id="wBall_1">
            <div className="wInnerBall"></div>
          </div>
          <div className="wBall" id="wBall_2">
            <div className="wInnerBall"></div>
          </div>
          <div className="wBall" id="wBall_3">
            <div className="wInnerBall"></div>
          </div>
          <div className="wBall" id="wBall_4">
            <div className="wInnerBall"></div>
          </div>
          <div className="wBall" id="wBall_5">
            <div className="wInnerBall"></div>
          </div>
        </div>
      </div>
    </CSSTransition>
  );
};
export default SpinnerWin;
