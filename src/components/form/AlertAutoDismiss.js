import React, { useEffect, useState } from 'react';
import { Alert } from 'react-bootstrap';
const AlertAutoDismiss = ({
  time,
  heading,
  text,
  handleCallback,
  ...props
}) => {
  const [show, setShow] = useState(true);
  useEffect(() => {
    console.log('AlertAutoDismiss', show);
    const timeout = setTimeout(() => {
      setShow(false);
      handleCallback();
    }, time);
    return () => {
      clearTimeout(timeout);
    };
  }, []);
  return (
    <Alert variant="success" show={show} dismissible>
      <Alert.Heading>Form succesfully</Alert.Heading>
      <p>{`Document id added`}</p>
    </Alert>
  );
};
export default AlertAutoDismiss;
