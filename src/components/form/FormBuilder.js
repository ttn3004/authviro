import React from 'react';

import TextElement from 'components/form/element/TextElement';
import GroupElement from 'components/form/element/GroupElement';
import LabelElement from 'components/form/element/LabelElement';
import SelectElement from 'components/form/element/SelectElement';
import ValidationElement from 'components/form/element/ValidationElement';
import DateTimeElement from 'components/form/element/DateTimeElement';
import FileElement from 'components/form/element/FileElement';
import InputGroupElement from 'components/form/element/InputGroupElement';
import RangeElement from 'components/form/element/RangeElement';
import CheckboxElement from 'components/form/element/CheckboxElement';
import SwitchElement from 'components/form/element/SwitchElement';

const FormBuilder = ({
  json,
  style,
  register,
  errors,
  validationStyle,
  btnsubmit,
  title,
  isDisableValidMsg,
  control,
  watch,
  reset,
  doc,
  getValues,
  setError,
  needEnableToChange,
  handleDeleteFile,
  ...props
}) => {
  const formContent = (
    <React.Fragment>
      {Object.keys(json).map((key) => {
        let contentEl = '';
        const el = json[key];
        // console.log('error-------', key, errors[key]);
        const groupStyle = el.group ? el.group : style.group;
        const labelStyle = el.label ? el.label : style.label;
        const controlStyle = el.control ? el.control : style.control;
        let name = el.name;
        const inputGroupClass = el.inputGroupClass;
        const inputGroupText = el.inputGroupText;
        const inputGroupPosition = el.inputGroupPosition
          ? el.inputGroupPosition
          : null;
        const readOnly = el.readOnly ? el.readOnly : false;
        const inverse = el.inverse;
        const controlGroup = el.controlGroup;
        const labelGroup = el.labelGroup;
        const valueReset = doc && doc[key] ? doc[key] : null;
        switch (el.type) {
          case 'text':
            contentEl = (
              <TextElement
                id={key}
                type={el.type}
                placeholder={el.placeholder}
                controlClass={controlStyle}
                validClass={errors[key] ? validationStyle.invalid : ''}
                register={register({ ...el.validation })}
                error={errors[key] ? errors[key] : null}
                readOnly={needEnableToChange || readOnly}
              />
            );
            break;
          case 'file':
            const fileWatch = watch(el.name);
            const fileName =
              fileWatch && fileWatch.length > 0 ? fileWatch : null;
            name = el.placeholder;
            contentEl = (
              <FileElement
                id={key}
                type={el.type}
                placeholder={el.placeholder}
                controlClass={controlStyle}
                validClass={errors[key] ? validationStyle.invalid : ''}
                register={register({ ...el.validation })}
                error={errors[key] ? errors[key] : null}
                controlGroup={controlGroup}
                labelGroup={labelGroup}
                file={fileName}
                multiple={el.multiple}
                valueReset={valueReset}
                readOnly={needEnableToChange || readOnly}
                doc={doc}
                handleDeleteFile={handleDeleteFile}
              />
            );
            break;
          case 'select':
            const collection = el.collection;
            const multiple = el.multiple;

            contentEl = (
              <SelectElement
                id={key}
                type={el.type}
                placeholder={el.placeholder}
                controlClass={controlStyle}
                validClass={errors[key] ? validationStyle.invalid : ''}
                register={register({ ...el.validation })}
                error={errors[key] ? errors[key] : null}
                multiple={multiple}
                collection={collection}
                valueReset={valueReset}
                control={control}
                rules={{ ...el.validation }}
                setError={setError}
                getValues={getValues}
                readOnly={needEnableToChange || readOnly}
              />
            );
            break;
          case 'checkbox':
            const valuesCheckbox = el.defvalue;
            contentEl = (
              <CheckboxElement
                id={key}
                type={el.type}
                placeholder={el.placeholder}
                controlClass={controlStyle}
                validClass={errors[key] ? validationStyle.invalid : ''}
                controlGroup={controlGroup}
                labelGroup={labelGroup}
                register={register({ ...el.validation })}
                error={errors[key] ? errors[key] : null}
                values={valuesCheckbox}
                readOnly={needEnableToChange || readOnly}
              />
            );
            break;
          case 'checkboxes':
            break;
          case 'radio':
            const valuesRadio = el.defvalue;
            contentEl = (
              <CheckboxElement
                id={key}
                type={el.type}
                placeholder={el.placeholder}
                controlClass={controlStyle}
                validClass={errors[key] ? validationStyle.invalid : ''}
                controlGroup={controlGroup}
                labelGroup={labelGroup}
                register={register({ ...el.validation })}
                error={errors[key] ? errors[key] : null}
                values={valuesRadio}
                readOnly={needEnableToChange || readOnly}
              />
            );
            break;
          case 'switch':
            contentEl = (
              <SwitchElement
                id={key}
                type={el.type}
                name={name}
                controlClass={controlStyle}
                validClass={errors[key] ? validationStyle.invalid : ''}
                controlGroup={controlGroup}
                labelGroup={labelGroup}
                register={register({ ...el.validation })}
                error={errors[key] ? errors[key] : null}
                readOnly={needEnableToChange || readOnly}
              />
            );
            break;
          case 'radios':
            break;
          case 'number':
            break;
          case 'textarea':
            break;
          case 'email':
            break;
          case 'range':
            contentEl = (
              <RangeElement
                id={key}
                type={el.type}
                placeholder={el.placeholder}
                controlClass={controlStyle}
                validClass={errors[key] ? validationStyle.invalid : ''}
                register={register({ ...el.validation })}
                error={errors[key] ? errors[key] : null}
                min={el.min}
                max={el.max}
                readOnly={needEnableToChange || readOnly}
              />
            );
            break;
          case 'search':
            break;
          case 'telephone':
            break;
          case 'url':
            break;
          case 'time':
            break;
          case 'datetime':
            contentEl = (
              <DateTimeElement
                id={key}
                type={el.type}
                placeholder={el.placeholder}
                controlClass={style.control}
                validClass={errors[key] ? validationStyle.invalid : ''}
                register={register({ ...el.validation })}
                error={errors[key] ? errors[key] : null}
                control={control}
                defvalue={el.defvalue}
                readOnly={readOnly}
              />
            );
            break;
          case 'datetime-local':
            break;
          case 'week':
            break;
          case 'month':
            break;
          default:
            break;
        }
        //console.log(contentEl);
        const groupEl = (
          <InputGroupElement
            id={key}
            inputGroupClass={inputGroupClass}
            validClass={errors[key] ? validationStyle.invalid : ''}
            inputGroupText={inputGroupText}
            inputGroupPosition={inputGroupPosition}
          >
            {contentEl}
          </InputGroupElement>
        );
        const labelEl = (
          <LabelElement labelClass={labelStyle} id={key} name={name} />
        );
        return (
          <GroupElement key={key} groupClass={groupStyle}>
            {inverse && (
              <React.Fragment>
                {groupEl} {labelEl}
              </React.Fragment>
            )}
            {!inverse && (
              <React.Fragment>
                {' '}
                {labelEl} {groupEl}
              </React.Fragment>
            )}
            {!isDisableValidMsg && (
              <ValidationElement
                validationClass={
                  errors[key] ? validationStyle.invalidfeedback : null
                }
                message={errors[key] ? errors[key].message : null}
              />
            )}
          </GroupElement>
        );
      })}
    </React.Fragment>
  );

  return <React.Fragment>{formContent}</React.Fragment>;
};

export default FormBuilder;
