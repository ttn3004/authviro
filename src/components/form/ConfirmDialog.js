import React, { useCallback } from 'react';
import FormFactory from 'components/form/FormFactory';
import { Modal, Button } from 'react-bootstrap';

const ConfirmDialog = ({
  title,
  show,
  handleClose,
  handleConfirm,
  ...props
}) => {
 
  return (
    <Modal size="lg" centered show={show} onHide={() => console.log()}>
      <Modal.Header>
        <Modal.Title>Confirm to delete</Modal.Title>
      </Modal.Header>
      <Modal.Body>{title}</Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
        <Button variant="primary" onClick={handleConfirm}>
          Save Changes
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
export default ConfirmDialog;
