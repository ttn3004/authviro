import React, { useEffect, useState, useCallback, useContext } from 'react';
import { useForm } from 'react-hook-form';
import FormElement from 'components/form/element/FormElement';
import { FirebaseAuth } from 'provider/AuthProvider';
import AlertAutoDismiss from 'components/form/AlertAutoDismiss';
const FormFactory = ({
  title,
  json,
  style,
  btnsubmit,
  firebaseCollection,
  dbModel,
  handleCallback,
  doc,
  idGen,
  needEnableToChange,
  handleEnableChange,
  ...props
}) => {
  const [editDoc, setEditDoc] = useState(doc);
  const [show, setShow] = useState(false);
  const { setLoading } = useContext(FirebaseAuth);
  useEffect(() => {
    const resetForm = () => {
      if (doc) {
        const mapVal = new Map();
        //reset({ name: 'ccc', url: 'ffff' });
        const resetValues = {};
        Object.keys(doc).map((key) => {
          if (json[key] && json[key].type && json[key].type !== 'file')
            mapVal.set(key, doc[key]);
        });
        mapVal.forEach(function (value, key) {
          resetValues[key] = value;
        });
        reset(resetValues);
      } else {
        reset();
      }
    };
    resetForm();
    // set multiple values
  }, [doc, show]);
  const formStyle = style.default
    ? style.default
    : style.row
    ? style.row
    : style.inline
    ? style.inline
    : style.horizontal;
  const {
    register,
    handleSubmit,
    errors,
    setError,
    control,
    watch,
    reset,
    setValue,
    getValues,
  } = useForm();

  const onSubmit = async (data, e) => {
    try {
      setLoading(true);
      let isUpdMode = false;
      if (doc) {
        //case update
        //const res = await dbModel.update(firebaseCollection, data, json);
        data.uid = doc.uid;
        isUpdMode = true;
      }
      //case add
      const res = await dbModel.save(
        firebaseCollection,
        data,
        json,
        idGen,
        doc
      );
      if (isUpdMode) setEditDoc(res.data());
      e.target.reset(); // reset form
      if (handleCallback) {
        handleCallback(res);
      }

      setLoading(false);
      setShow(true);
    } catch (error) {
      console.error(error);
      setLoading(false);
    }
  };

  const handleDeleteFile = useCallback(
    async (field, file) => {
      try {
        setLoading(true);
        const docRes = await dbModel.deleteFile(
          firebaseCollection,
          field,
          file,
          doc
        );
        setEditDoc(docRes.data());
        //console.log('handleDeleteFile newDoc', docRes.data());
        setLoading(false);
        setShow(true);
      } catch (error) {
        console.error(error);
        setLoading(false);
      }
    },
    [editDoc]
  );
  const handleCallbackAlert = useCallback(() => {
    console.log('FormFactory handleCallbackAlert');
    setShow(false);
  }, [show]);
  return (
    <React.Fragment>
      <FormElement
        title={title}
        json={json}
        style={style}
        register={register}
        errors={errors}
        formStyle={formStyle}
        handleSubmit={handleSubmit(onSubmit)}
        btnsubmit={btnsubmit}
        control={control}
        watch={watch}
        reset={reset}
        doc={editDoc}
        setValue={setValue}
        getValues={getValues}
        setError={setError}
        needEnableToChange={needEnableToChange}
        handleEnableChange={handleEnableChange}
        handleDeleteFile={handleDeleteFile}
      />
      {show && (
        <AlertAutoDismiss
          time="2000"
          heading="Form successfully"
          text={`Document is in collection  ${firebaseCollection}`}
          handleCallback={handleCallbackAlert}
        />
      )}
    </React.Fragment>
  );
};

export default FormFactory;
