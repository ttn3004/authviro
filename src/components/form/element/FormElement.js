import React from 'react';
import RowElement from 'components/form/element/RowElement';
import FormBuilder from 'components/form/FormBuilder';
import GroupElement from 'components/form/element/GroupElement';
import FormValidation from 'components/form/FormValidation';
import { CSSTransition } from 'react-transition-group';
import 'components/transition/csstransition.css';
const FormElement = ({
  title,
  json,
  style,
  register,
  errors,
  formStyle,
  handleSubmit,
  btnsubmit,
  control,
  watch,
  reset,
  doc,
  setValue,
  getValues,
  setError,
  needEnableToChange,
  handleEnableChange,
  handleDeleteFile,
  ...props
}) => {
  const validPosition = formStyle.validPosition
    ? formStyle.validPosition
    : null;

  return (
    <React.Fragment>
      <FormValidation
        validPosition={validPosition}
        validationClass={style.validationStyle.invalidfeedback}
        errors={errors}
      >
        <form className={formStyle.classname} onSubmit={handleSubmit}>
          <RowElement rowClass={formStyle.rowClass}>
            <FormBuilder
              json={json}
              validationStyle={style.validationStyle}
              style={formStyle}
              register={register}
              errors={errors}
              isDisableValidMsg={validPosition ? true : false}
              control={control}
              watch={watch}
              reset={reset}
              doc={doc}
              getValues={getValues}
              setError={setError}
              needEnableToChange={needEnableToChange}
              handleDeleteFile={handleDeleteFile}
            />
          </RowElement>
          <GroupElement groupClass={formStyle.group}>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <button type="submit" className={btnsubmit.classname}>
                {btnsubmit.label}
              </button>
              {needEnableToChange && (
                <CSSTransition
                  in={needEnableToChange}
                  timeout={500}
                  classNames="generic-item"
                >
                  <button
                    type="button"
                    className="btn btn-warning"
                    onClick={() => handleEnableChange()}
                  >
                    Enable change
                  </button>
                </CSSTransition>
              )}
            </div>
          </GroupElement>
        </form>
      </FormValidation>
    </React.Fragment>
  );
};
export default FormElement;
