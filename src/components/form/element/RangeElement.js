import React from 'react';
const RangeElement = ({
  id,
  type,
  placeholder,
  controlClass,
  validClass,
  register,
  readOnly,
  min,
  max,
  error,
  ...props
}) => {
  return (
    <React.Fragment>
      <input
        className={`${controlClass} ${validClass}`}
        id={id}
        type={type}
        placeholder={placeholder}
        name={id}
        ref={register}
        readOnly={readOnly}
        min={min}
        max={max}
      />
    </React.Fragment>
  );
};
export default RangeElement;
