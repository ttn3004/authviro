import React from 'react';
import ReactDatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { Controller } from 'react-hook-form';
import 'components/form/form.css';
import { dateFormat } from 'config/modelConfig.js';
import fr from 'date-fns/locale/fr';
import { registerLocale, setDefaultLocale } from 'react-datepicker';
registerLocale('fr', fr);
const DateTimeElement = ({
  id,
  type,
  placeholder,
  controlClass,
  validClass,
  register,
  error,
  control,
  defvalue,
  readOnly,
  ...props
}) => {
  const defaultValue = defvalue ? eval(defvalue) : null;
  return (
    <div style={{ display: 'block' }}>
      <Controller
        id={id}
        name={id}
        as={ReactDatePicker}
        control={control}
        valueName="selected" // DateSelect value's name is selected
        className={`${controlClass} ${validClass}`}
        placeholderText={placeholder}
        defaultValue={defaultValue}
        dateFormat={dateFormat.format}
        locale="fr"
        readOnly={true}
      />
    </div>
  );
};
export default DateTimeElement;
