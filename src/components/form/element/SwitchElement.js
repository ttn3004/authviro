import React from 'react';
const SwitchElement = ({
  id,
  type,
  name,
  controlClass,
  validClass,
  labelGroup,
  register,
  readOnly,
  values,
  controlGroup,
  disabled,
  error,
  ...props
}) => {
  return (
    <div className={controlGroup} key={id}>
      <input
        className={`${controlClass} ${validClass}`}
        id={id}
        type="checkbox"
        name={id}
        ref={register}
        disabled={disabled || readOnly}
      />
      <label
        className={labelGroup}
        htmlFor={id}
        disabled={disabled || readOnly}
      >
        {name}
      </label>
    </div>
  );
};
export default SwitchElement;
