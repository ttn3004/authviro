import React from 'react';
const CheckboxElement = ({
  id,
  type,
  placeholder,
  controlClass,
  validClass,
  labelGroup,
  register,
  readOnly,
  values,
  controlGroup,
  error,
  ...props
}) => {
  return (
    <React.Fragment>
      {values.map((e) => {
        return (
          <div className={controlGroup} key={e.uid}>
            <input
              className={`${controlClass} ${validClass}`}
              id={id}
              type={type}
              placeholder={placeholder}
              name={id}
              ref={register}
              readOnly={readOnly}
              value={e.value}
            />
            <label className={labelGroup} htmlFor={id}>
              {e.value}
            </label>
          </div>
        );
      })}
    </React.Fragment>
  );
};
export default CheckboxElement;
