import React from 'react';
import FileListItemElement from 'components/form/element/FileListItemElement';
import { random } from 'util/NumberUtil';
const FileListElement = ({
  field,
  valueReset,
  doc,
  handleDeleteFile,
  ...props
}) => {
  return (
    <React.Fragment>
      {valueReset && (
        <React.Fragment>
          {valueReset.map((element) => {
            return (
              <FileListItemElement
                key={random(1, Date.now())}
                field={field}
                doc={doc}
                file={element}
                handleDeleteFile={handleDeleteFile}
              />
            );
          })}
        </React.Fragment>
      )}
    </React.Fragment>
  );
};
export default FileListElement;
