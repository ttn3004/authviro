import React from 'react';
const InputGroupElement = ({
  id,
  inputGroupClass,
  inputGroupText,
  inputGroupPosition,
  validClass,
  ...props
}) => {
  const createMarkup = (str) => {
    return { __html: str };
  };
  const prepend = (
    <div className="input-group-prepend">
      <span
        className="input-group-text"
        id={id}
        dangerouslySetInnerHTML={createMarkup(inputGroupText)}
      ></span>
    </div>
  );
  if (inputGroupClass) {
    return (
      <React.Fragment>
        <div className={`${inputGroupClass} ${validClass}`}>
          {inputGroupPosition === 'left' && (
            <React.Fragment>
              {prepend} {props.children}
            </React.Fragment>
          )}

          {inputGroupPosition === 'right' && (
            <React.Fragment>
              {props.children} {prepend}
            </React.Fragment>
          )}
        </div>
      </React.Fragment>
    );
  }
  return <React.Fragment>{props.children}</React.Fragment>;
};
export default InputGroupElement;
