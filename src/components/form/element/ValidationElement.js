import React from 'react';

const ValidationElement = ({ validationClass, message, ...props }) => {
  return (
    <React.Fragment>
      {validationClass && <div className={validationClass}>{message}</div>}
    </React.Fragment>
  );
};
export default ValidationElement;
