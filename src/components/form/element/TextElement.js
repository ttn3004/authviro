import React from 'react';
const TextElement = ({
  id,
  type,
  placeholder,
  controlClass,
  validClass,
  register,
  readOnly,
  error,
  ...props
}) => {
  return (
    <React.Fragment>
      <input
        className={`${controlClass} ${validClass}`}
        id={id}
        type={type}
        placeholder={placeholder}
        name={id}
        ref={register}
        readOnly={readOnly}
      />
    </React.Fragment>
  );
};
export default TextElement;
