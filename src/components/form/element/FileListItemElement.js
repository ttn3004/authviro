import React, { useState, useCallback } from 'react';
import { BsFillTrashFill } from 'react-icons/bs';
import ConfirmDialog from 'components/form/ConfirmDialog';
const FileListItemElement = ({
  field,
  file,
  doc,
  handleDeleteFile,
  ...props
}) => {
  const [show, setShow] = useState(false);
  //console.log('file reset', valueReset);
  const handleConfirmDeleteFile = () => {
    setShow(true);
  };
  const handleDelete = useCallback(async () => {
    if (file) {
      //await dbModel.delete(doc, collectionName, json);
      const arrFile = doc[field];
      if (arrFile) {
        for (let i = 0; i < arrFile.length; i++) {
          const f = arrFile[i];
          if (f.ref === file.ref) {
            handleDeleteFile(field, f);
            break;
          }
        }
      }
      handleClose();
    }
  }, []);
  const handleClose = useCallback(() => {
    setShow(false);
  }, []);
  return (
    <React.Fragment>
      <button
        type="button"
        className="btn btn-outline-primary m-1"
        onClick={(element) => handleConfirmDeleteFile(element)}
      >
        {file.name}
        <BsFillTrashFill className="ml-2" />
      </button>

      <ConfirmDialog
        show={show}
        title={
          file
            ? `File ${file.name} for document ${doc.uid} will be deleted`
            : ''
        }
        handleConfirm={handleDelete}
        handleClose={handleClose}
      />
    </React.Fragment>
  );
};
export default FileListItemElement;
