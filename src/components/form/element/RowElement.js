import React from 'react';

const RowElement = ({ rowClass, ...props }) => {
  if (rowClass) {
    return (
      <React.Fragment>
        <div className={rowClass}>{props.children}</div>
      </React.Fragment>
    );
  }

  return <React.Fragment>{props.children}</React.Fragment>;
};
export default RowElement;
