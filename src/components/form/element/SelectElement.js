import React, { useEffect, useState } from 'react';
import { getUserCollection } from 'firebase/refmethods';
import { Controller } from 'react-hook-form';

const SelectElement = ({
  id,
  type,
  placeholder,
  controlClass,
  validClass,
  register,
  error,
  multiple,
  readOnly,
  collection,
  valueReset,
  control,
  rules,
  valueSelect,
  getValues,
  setError,
  ...props
}) => {
  const [optionvalues, setoptionvalues] = useState(null);
  const [defaultValue, setdefaultValue] = useState(
    valueReset ? valueReset : multiple ? [] : ''
  );
  const handleChange = (e) => {
    if (multiple) {
      let values = Array.from(
        e.target.selectedOptions,
        (option) => option.value
      );
      setdefaultValue(values);
      return values;
    } else {
      const val = e.target.value;
      setdefaultValue(val);
      return val;
    }
  };
  useEffect(() => {
    async function getOptions() {
      const result = await getUserCollection(collection);
      console.log('select', id, result);
      setoptionvalues(result);
    }
    if (collection) {
      if (Array.isArray(collection)) {
        setoptionvalues(collection);
      } else {
        getOptions();
      }
    }
  }, []);
  const optionsContent = (
    <React.Fragment>
      {!valueReset && <option hidden>{placeholder}</option>}
      {optionvalues && (
        <React.Fragment>
          {optionvalues.map((item) => {
            return (
              <option key={item.uid} value={item.value}>
                {item.value}
              </option>
            );
          })}
        </React.Fragment>
      )}
    </React.Fragment>
  );
  return (
    <Controller
      as={<select>{optionsContent}</select>}
      control={control}
      multiple={multiple}
      id={id}
      type={type}
      className={`${controlClass} ${validClass}`}
      name={id}
      readOnly={readOnly}
      defaultValue={defaultValue}
      rules={{ required: true }}
      onChange={([e]) => handleChange(e)}
    />
  );
};
export default SelectElement;
