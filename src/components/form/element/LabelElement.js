import React, { useEffect } from 'react';

const LabelElement = ({ labelClass, id, name, ...props }) => {
  return (
    <React.Fragment>
      <label className={labelClass} htmlFor={id}>
        {name}
      </label>
    </React.Fragment>
  );
};
export default LabelElement;
