import React from 'react';
import FileListElement from 'components/form/element/FileListElement';
import { BsFileEarmarkPlus } from 'react-icons/bs';

const FileElement = ({
  id,
  type,
  placeholder,
  controlClass,
  validClass,
  register,
  error,
  controlGroup,
  labelGroup,
  multiple,
  file,
  valueReset,
  reset,
  readOnly,
  doc,
  handleDeleteFile,
  ...props
}) => {
  const contentFiles = file ? (
    <React.Fragment>
      {Object.keys(file).map((key) => {
        const f = file[key];
        return (
          <button
            type="button"
            key={f.name}
            className="btn btn-outline-info my-1"
          >
            {f.name}
            <BsFileEarmarkPlus className="ml-2" />
          </button>
        );
      })}
    </React.Fragment>
  ) : (
    ''
  );
  return (
    <React.Fragment>
      <div className={controlGroup}>
        <input
          className={`${controlClass} ${validClass}`}
          id={id}
          type={type}
          name={id}
          ref={register}
          multiple={multiple}
          aria-describedby={id}
          readOnly={readOnly}
        />
        <label className={labelGroup} htmlFor={id}>
          {placeholder}
        </label>
        {contentFiles}
      </div>
      {valueReset && (
        <div className={controlGroup}>
          <hr />
          <FileListElement
            field={id}
            valueReset={valueReset}
            doc={doc}
            handleDeleteFile={handleDeleteFile}
          />
        </div>
      )}
    </React.Fragment>
  );
};
export default FileElement;
