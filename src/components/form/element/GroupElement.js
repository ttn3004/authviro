import React, { useEffect } from 'react';

const GroupElement = ({ groupClass, ...props }) => {
  if (groupClass) {
    return (
      <React.Fragment>
        {groupClass && <div className={groupClass}>{props.children}</div>}
      </React.Fragment>
    );
  }

  return <React.Fragment>{props.children}</React.Fragment>;
};
export default GroupElement;
