import React from 'react';

const FormValidation = ({
  validPosition,
  validationClass,
  errors,
  ...props
}) => {
  const msgValidation = (
    <React.Fragment>
      <div>
        {Object.keys(errors).map((key) => {
          const msg = errors[key].message;
          return (
            <p
              className={validationClass}
              style={{ display: 'block' }}
              key={key}
            >
              {msg}
            </p>
          );
        })}
      </div>
    </React.Fragment>
  );
  return (
    <React.Fragment>
      {validPosition === 'top' &&
        errors &&
        Object.keys(errors).length > 0 &&
        msgValidation}
      {props.children}
      {validPosition === 'bottom' &&
        errors &&
        Object.keys(errors).length > 0 &&
        msgValidation}
    </React.Fragment>
  );
};
export default FormValidation;
