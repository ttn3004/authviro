import React, { useContext, useEffect, useState, useRef } from 'react';
import { FirebaseAuth } from 'provider/AuthProvider';
import { db } from 'firebase/firebaseIndex';
import { withRouter, Link } from 'react-router-dom';
const GenericNotFound = (props) => {
  return <div>No Found</div>;
};
export default GenericNotFound;
