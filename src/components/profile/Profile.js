import React, { useEffect, useState, useCallback, useContext } from 'react';
import {
  firebaseCollection,
  formAdd,
  formAddModelStyle,
  formAddModelBtnSubmit,
  formAddModelTitle,
  needEnableToChange,
} from 'config/profileConfig.js';
import GenericOne from 'components/generic/GenericOne';
import { genericModel } from 'firebase/dbmethods';
import { FirebaseAuth } from 'provider/AuthProvider';
import { CSSTransition } from 'react-transition-group';
import 'components/transition/csstransition.css';
const Profile = ({ submenu, ...props }) => {
  const { user, profile, setProfile, response, setResponse } = useContext(
    FirebaseAuth
  );
  const [readOnly, setReadOnly] = useState(
    needEnableToChange ? needEnableToChange : false
  );

  const handleEnableChange = useCallback((res) => {
    setReadOnly(false);
  }, []);

  const handleCallback = useCallback((res) => {
    if (res && res.data()) {
      setProfile(res.data());
      setReadOnly(true);
    }
  }, []);
  return (
    <React.Fragment>
      <CSSTransition
        in={user ? true : false}
        timeout={500}
        classNames="generic-item"
      >
        <GenericOne
          dbModel={genericModel}
          firebaseCollection={firebaseCollection}
          json={formAdd}
          title={formAddModelTitle}
          addStyle={formAddModelStyle}
          btnsubmit={formAddModelBtnSubmit}
          doc={profile}
          idGen={user.uid}
          handleCallback={handleCallback}
          needEnableToChange={readOnly}
          handleEnableChange={handleEnableChange}
        />
      </CSSTransition>
    </React.Fragment>
  );
};

export default Profile;
