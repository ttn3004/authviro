import React, { useMemo, useEffect } from 'react';
import { random } from 'util/NumberUtil';
import { getRandomGridItems, findMinAttr, findMaxAttr } from 'util/GridUtil';
import { Carousel } from 'react-bootstrap';
import GridGenerator from 'components/grid/GridGenerator';

const CarouselGridGenerator = ({ nbItem, ...props }) => {
  //const itemsGenerator = useMemo(() => getRandomGridItems(10), []);

  let arrGrid = useMemo(() => arrGridGen(), []);
  function arrGridGen() {
    const arr = [];
    for (let i = 0; i < nbItem; i++) {
      const itemsGenerator = getRandomGridItems(random(6, 10));
      const grid = <GridGenerator items={itemsGenerator} />;
      arr.push(grid);
    }
    return arr;
  }
  return (
    <React.Fragment>
      {arrGrid && (
        <Carousel
          touch={true}
          
        >
          {arrGrid.map((e) => {
            return (
              <Carousel.Item key={random(1, Date.now())}>
                {e}
                <Carousel.Caption>
                  <h3>{e.name}</h3>
                  <p>{e.description}</p>
                </Carousel.Caption>
              </Carousel.Item>
            );
          })}
        </Carousel>
      )}
    </React.Fragment>
  );
};
export default CarouselGridGenerator;
