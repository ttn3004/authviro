import React, { useMemo, useEffect } from 'react';
import { getRandomGridItems, findMinAttr, findMaxAttr } from 'util/GridUtil';
import { Carousel } from 'react-bootstrap';

const CarouselGenerator = ({ items, ...props }) => {
  const itemsGenerator = useMemo(() => getRandomGridItems(10), []);

  const minHeight = useMemo(() => findMaxAttr(itemsGenerator, 'height'), []);

  return (
    <React.Fragment>
      {itemsGenerator && minHeight.height > 0 && (
        <Carousel
          touch={true}
          style={{
            height: `${minHeight.height}px`,
            width: '100%',
          }}
        >
          {itemsGenerator.map((e) => {
            return (
              <Carousel.Item
                key={e.uid}
                style={{
                  height: `${minHeight.height}px`,
                  width: '100%',
                  backgroundImage: `url("${e.url}")`,
                  backgroundPosition: 'center',
                  backgroundRepeat: 'no-repeat',
                  backgroundSize: 'cover',
                }}
              >
                <Carousel.Caption>
                  <h3>{e.name}</h3>
                  <p>{e.description}</p>
                </Carousel.Caption>
              </Carousel.Item>
            );
          })}
        </Carousel>
      )}
    </React.Fragment>
  );
};
export default CarouselGenerator;
