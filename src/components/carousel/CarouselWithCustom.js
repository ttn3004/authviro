import React, { useMemo, useState } from 'react';
import { getRandomGridItems, findMinAttr, findMaxAttr } from 'util/GridUtil';
import { Carousel } from 'react-bootstrap';
import { BsChevronLeft, BsChevronRight } from 'react-icons/bs';
import { carousel } from 'config/styleConfig';
const CarouselWithCustom = ({ items, ...props }) => {
  const itemsGenerator = useMemo(() => getRandomGridItems(10), []);

  const minHeight = useMemo(() => findMaxAttr(itemsGenerator, 'height'), []);
  const [activeIndex, setActiveIndex] = useState(0);
  const [direction, setDirection] = useState(null);
  const goToIndex = (idx) => {
    console.log(idx);
    setActiveIndex(idx);
  };
  const handleSelect = (selectedIndex, e) => {
    setActiveIndex(selectedIndex);
  };
  // method called to handle toggle (next/prev)
  const toggleCarousel = (direction) => {
    let index = activeIndex;
    const [min, max] = [0, itemsGenerator.length - 1];

    if (direction === 'next') {
      index++;
    } else if (direction === 'prev') {
      index--;
    }

    if (index > max) {
      // at max, start from top
      index = 0;
    }
    if (index < min) {
      // at min, start from max
      index = max;
    }
    setDirection(direction);
    setActiveIndex(index);
  };

  return (
    <React.Fragment>
      {itemsGenerator && minHeight.height > 0 && (
        <Carousel
          activeIndex={activeIndex}
          onSelect={handleSelect}
          touch={true}
          style={{
            height: `${minHeight.height}px`,
            width: '100%',
          }}
          indicators={false}
          controls={false}
          direction={direction}
        >
          {itemsGenerator.map((e) => {
            return (
              <Carousel.Item
                key={e.uid}
                style={{
                  height: `${minHeight.height}px`,
                  width: '100%',
                  backgroundImage: `url("${e.url}")`,
                  backgroundPosition: 'center',
                  backgroundRepeat: 'no-repeat',
                  backgroundSize: 'cover',
                }}
              >
                <Carousel.Caption>
                  <h3>{e.name}</h3>
                  <p>{e.description}</p>
                </Carousel.Caption>
              </Carousel.Item>
            );
          })}
          <ol className="carousel-indicators">
            {itemsGenerator.map((e, index) => {
              return (
                <li
                  key={index}
                  onClick={() => goToIndex(index)}
                  className={activeIndex === index ? 'active' : ''}
                  style={{
                    border: carousel.borderThumb,
                    height: carousel.heightThumb,
                    width: '100%',
                    backgroundImage: `url("${e.url}")`,
                    backgroundPosition: 'center',
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'cover',
                  }}
                />
              );
            })}
          </ol>
          <a
            href="#/"
            className="carousel-control-prev"
            role="button"
            onClick={() => toggleCarousel('prev')}
          >
            <BsChevronLeft className="carousel-control-prev-icon" />
          </a>
          <a
            href="#/"
            className="carousel-control-next"
            role="button"
            onClick={() => toggleCarousel('next')}
          >
            <BsChevronRight className="carousel-control-next-icon" />
          </a>
        </Carousel>
      )}
    </React.Fragment>
  );
};
export default CarouselWithCustom;
