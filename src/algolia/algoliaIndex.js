import algoliasearch from 'algoliasearch';
// configure algolia
const algolia = algoliasearch(
  process.env.REACT_APP_ALGOLIA_ID,
  process.env.REACT_APP_ALGOLIA_ADMIN_KEY
);
const search = algoliasearch(
  process.env.REACT_APP_ALGOLIA_ID,
  process.env.REACT_APP_ALGOLIA_KEY
);
export const algoliaIndex = algolia.initIndex(
  process.env.REACT_APP_ALGOLIA_AME
);
export const algoliaSearch = search.initIndex(
  process.env.REACT_APP_ALGOLIA_AME
);
