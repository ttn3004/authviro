export const firebaseCollection = 'profile';
export const filter = null;
export const orderCollection = null;
export const needEnableToChange = true;
export const formAdd = {
  name: {
    type: 'text',
    name: 'User name',
    placeholder: 'Enter your User name',
    defvalue: '',
    validation: {
      required: 'User name is required',
      max: null,
      min: null,
      maxLength: null,
      minLength: null,
      pattern: null,
    },
  },
  lasName: {
    type: 'text',
    name: 'Last name',
    placeholder: 'Enter your last name',
    defvalue: '',
    validation: {
      required: 'Last name is required',
      max: null,
      min: null,
      maxLength: null,
      minLength: null,
      pattern: null,
    },
    inputGroupPosition: 'left',
    inputGroupClass: 'input-group',
    inputGroupText: '@',
  },
  telephone: {
    type: 'text',
    name: 'Your phone',
    placeholder: 'Enter your phone',
    defvalue: '',
    validation: {
      required: null,
      max: null,
      min: null,
      maxLength: null,
      minLength: null,
      pattern: null,
    },
    inputGroupPosition: 'left',
    inputGroupClass: 'input-group',
    inputGroupText: '&#9742;',
  },
  address: {
    type: 'text',
    name: 'Address',
    placeholder: 'Enter your address',
    defvalue: '',
    validation: {
      required: 'Address is required',
      max: null,
      min: null,
      maxLength: null,
      minLength: null,
      pattern: null,
    },
    inputGroupPosition: 'left',
    inputGroupClass: 'input-group',
    inputGroupText: '&#127962;&#65039;',
  },
  profession: {
    type: 'text',
    name: 'You profession',
    placeholder: 'Enter your aprofession',
    defvalue: '',
    validation: {
      required: null,
      max: null,
      min: null,
      maxLength: null,
      minLength: null,
      pattern: null,
    },
    inputGroupPosition: 'left',
    inputGroupClass: 'input-group',
    inputGroupText: '&#127962;&#65039;',
  },
  dateCreate: {
    type: 'datetime',
    name: 'Creation date',
    classname: 'form-group',
    placeholder: 'Enter your date',
    defvalue: 'Date.now()',
    validation: {
      required: false,
      max: null,
      min: null,
      maxLength: null,
      minLength: null,
      pattern: null,
    },
  },
};
export const formAddModelTitle = {
  label: 'Your profile',
  classname: 'h1',
};
export const formAddModelBtnSubmit = {
  label: 'Submit',
  classname: 'btn btn-primary',
};
export const formAddModelStyle = {
  default: {
    type: 'default',
    classname: null,
    group: 'form-group',
    label: null,
    control: 'form-control',
    small: 'form-text text-muted',
    validPosition: null,
    rowClass: null,
  },
  validationStyle: {
    valid: 'is-valid',
    invalid: 'is-invalid',
    validfeedback: 'valid-feedback',
    invalidfeedback: 'invalid-feedback',
  },
};
export const dateFormat = {
  format: 'dd MMM yyyy HH:mm:ss',
};
