import MarketAddModel from 'components/market/MarketAddModel';
import Market from 'components/market/Market';
import Category from 'components/category/Category';
import Profile from 'components/profile/Profile';
import GenericOne from 'components/generic/GenericOne';
export const language = 'fr';
export const collectionsMenu = [
  {
    key: 1,
    name: 'Market',
    to: '/market',
    component: Market,
  },
  {
    name: 'Add model',
    to: '/market/add',
    component: MarketAddModel,
    parent: 1,
  },
  {
    key: 2,
    name: 'Category',
    to: '/category',
    component: Category,
  },
  {
    name: 'Add category',
    to: '/category',
    type: 'add',
    parent: 2,
  },
  {
    name: 'Update',
    to: '/category',
    type: 'update',
    parent: 2,
  },
  {
    key: 3,
    name: 'Profile',
    to: '/profile',
    component: Profile,
  },
  {
    name: 'Infos',
    to: '/profile/info',
    component: GenericOne,
    parent: 3,
  },
];
/**authentification and user profile config */
export const needEmailVerification = false;
export const needPhoneVerification = false;
export const profileConfig = {
  profileCollection: 'profile',
};
export const algolia = {
  indexed: true,
  search: {
    labelKey: 'value',
    toRetrieve: ['value'],
    hitsPerPage: 10,
    placeholder: 'Search from Algolia',
    minLength: 2,
  },
};
