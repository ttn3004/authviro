export const firebaseCollection = 'model';
//export const paging = null;
export const paging = {
  itemsPerPage: 5,
};
export const filterCollection = {
  name: {
    type: 'text',
    name: 'Model name',
    placeholder: 'Enter your model name',
    multiple: false,
    operator: '==',
  },
  category: {
    type: 'select',
    name: 'Category',
    placeholder: 'Enter your category',
    collection: 'category',
    multiple: false,
    operator: '==',
  },
};
export const orderCollection = {
  name: {
    type: 'text',
    operator: 'asc',
    label: 'Name',
  },
};
export const needConfirmToDelete = true;
export const formAddModel = {
  name: {
    type: 'text',
    name: 'Model name',
    placeholder: 'Enter your model name',
    defvalue: '',
    validation: {
      required: 'Model name is required',
      max: null,
      min: null,
      maxLength: null,
      minLength: null,
      pattern: null,
    },
    inputGroupPosition: 'left',
    inputGroupClass: 'input-group',
    inputGroupText: '@',
  },
  category: {
    type: 'select',
    name: 'Category',
    placeholder: 'Enter your model category',
    defvalue: '',
    validation: {
      required: 'Model category is required',
      max: null,
      min: null,
      maxLength: null,
      minLength: null,
      pattern: null,
    },
    collection: 'category',
    multiple: false,
  },
  keywords: {
    type: 'select',
    name: 'Keywords',
    placeholder: 'Choose your model keywords',
    defvalue: '',
    validation: {
      required: null,
      max: null,
      min: null,
      maxLength: null,
      minLength: null,
      pattern: null,
    },
    multiple: true,
    collection: [
      { uid: 1, value: 'option 1' },
      { uid: 2, value: 'option 2' },
      { uid: 4, value: 'option 4' },
      { uid: 5, value: 'option 5' },
    ],
  },
  dimension: {
    type: 'range',
    name: 'Dimension',
    placeholder: 'Choose your model dimension',
    defvalue: '',
    validation: {
      required: null,
      max: null,
      min: null,
      maxLength: null,
      minLength: null,
      pattern: null,
    },
    min: 0,
    max: 10,
    control: 'form-control-range',
  },

  url: {
    type: 'text',
    name: 'url',
    placeholder: 'Enter your url',
    defvalue: '',
    validation: {
      required: null,
      max: null,
      min: null,
      maxLength: null, //{ value: 3, message: 'Only 3 character allowed' },
      minLength: null,
      pattern: null,
    },
  },
  display: {
    type: 'checkbox',
    name: 'Display',
    placeholder: 'Choose your model display type',
    defvalue: [
      { uid: 1, value: 'option 1' },
      { uid: 2, value: 'option 2' },
      { uid: 3, value: 'option 3' },
    ],
    validation: {
      required: null,
      max: null,
      min: null,
      maxLength: null,
      minLength: null,
      pattern: null,
    },
    controlGroup: 'form-check',
    labelGroup: 'form-check-label',
    control: 'form-check-input',
  },
  radio: {
    type: 'radio',
    name: 'radio',
    placeholder: 'Choose your model radio type',
    defvalue: [
      { uid: 1, value: 'radio 1' },
      { uid: 2, value: 'radio 2' },
      { uid: 3, value: 'radio 3' },
    ],
    validation: {
      required: null,
      max: null,
      min: null,
      maxLength: null,
      minLength: null,
      pattern: null,
    },
    controlGroup: 'form-check',
    labelGroup: 'form-check-label',
    control: 'form-check-input',
  },
  file: {
    type: 'file',
    name: 'file',
    placeholder: 'Choose a file from local',
    defvalue: '',
    validation: {
      required: null,
      max: null,
      min: null,
      maxLength: null,
      minLength: null,
      pattern: null,
    },
    control: 'custom-file-input',
    controlGroup: 'custom-file',
    labelGroup: 'custom-file-label',
    multiple: true,
    hosting: 'model/',
  },
  switch: {
    type: 'switch',
    name: 'Model switch',
    placeholder: 'Enter your model switch',
    defvalue: '',
    validation: {
      required: false,
      max: null,
      min: null,
      maxLength: null,
      minLength: null,
      pattern: null,
    },
    controlGroup: 'custom-control custom-switch',
    control: 'custom-control-input',
    labelGroup: 'custom-control-label',
  },
  dateCreate: {
    type: 'datetime',
    name: 'Creation date',
    classname: 'form-group',
    placeholder: 'Enter your date',
    defvalue: 'Date.now()',
    validation: {
      required: false,
      max: null,
      min: null,
      maxLength: null,
      minLength: null,
      pattern: null,
    },
  },
};
export const formAddModelTitle = {
  label: 'Add Model',
  classname: 'h1',
};
export const formAddModelBtnSubmit = {
  label: 'Submit',
  classname: 'btn btn-primary',
};
export const formAddModelStyle = {
  default: {
    type: 'default',
    classname: null,
    group: 'form-group',
    label: null,
    control: 'form-control',
    small: 'form-text text-muted',
    validPosition: null,
    rowClass: null,
  },
  /*   inline: {
    type: 'inline',
    classname: 'form-inline',
    group: '',
    label: 'sr-only',
    control: 'form-control my-1 mr-sm-2',
    small: 'form-text text-muted',
    validPosition: 'bottom',
    rowClass: null,
  }, */

  /*   row: {
    type: 'row',
    classname: 'form-row',
    group: 'col-md-6 mb-3',
    control: 'form-control',
    small: 'form-text text-muted',
    validPosition: null,
    rowClass: 'form-row',
  }, */
  /*
  horizontal: {
    type: 'horizontal',
    classname: null,
    group: 'form-group row',
    label: 'col-sm-2 col-form-label',
    control: 'form-control col-sm-10',
    small: 'form-text text-muted',
    horizontalValid: 'col-sm-10',
      validPosition: 'bottom',
    rowClass: 'form-row',
  },*/
  validationStyle: {
    valid: 'is-valid',
    invalid: 'is-invalid',
    validfeedback: 'valid-feedback',
    invalidfeedback: 'invalid-feedback',
  },
};
export const dateFormat = {
  format: 'dd MMM yyyy HH:mm:ss',
};
