export const firebaseCollection = 'category';
export const filter = null;
export const orderCollection = {
  value: {
    type: 'text',
    operator: 'asc',
    label: 'Name',
  },
};
export const needConfirmToDelete = false;
export const formAddCategory = {
  value: {
    type: 'text',
    name: 'Category name',
    placeholder: 'Enter your Category name',
    defvalue: '',
    validation: {
      required: 'Category name is required',
      max: null,
      min: null,
      maxLength: null,
      minLength: null,
      pattern: null,
    },
    inputGroupPosition: 'left',
    inputGroupClass: 'input-group',
    inputGroupText: '@',
  },
  dateCreate: {
    type: 'datetime',
    name: 'Creation date',
    classname: 'form-group',
    placeholder: 'Enter your date',
    defvalue: 'Date.now()',
    validation: {
      required: false,
      max: null,
      min: null,
      maxLength: null,
      minLength: null,
      pattern: null,
    },
  },
};
export const formAddModelTitle = {
  label: 'Add Model',
  classname: 'h1',
};
export const formAddModelBtnSubmit = {
  label: 'Submit',
  classname: 'btn btn-primary',
};
export const formAddModelStyle = {
  default: {
    type: 'default',
    classname: null,
    group: 'form-group',
    label: null,
    control: 'form-control',
    small: 'form-text text-muted',
    validPosition: null,
    rowClass: null,
  },
  validationStyle: {
    valid: 'is-valid',
    invalid: 'is-invalid',
    validfeedback: 'valid-feedback',
    invalidfeedback: 'invalid-feedback',
  },
};
export const dateFormat = {
  format: 'dd MMM yyyy HH:mm:ss',
};
