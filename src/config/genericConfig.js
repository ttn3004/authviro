export const pageStyle = {
  menu: {
    nav: 'nav justify-content-center',
    item: 'nav-item',
    link: 'nav-link',
    active: 'active',
  },
  filter: {
    type: 'select',
  },
  list: {
    class: 'row row-cols-1 row-cols-md-3',
  },
  listItem: {
    class: 'col mb-4',
    card: {
      class:'card',
      image: {
        class: 'bd-placeholder-img card-img-top',
        width: '100%',
        height: '200',
        title: 'image',
      },
      body: {
        class: 'card-body',
        title: 'card-title',
        text: 'card-text',
        small: 'text-muted',
      },
      footer: {},
    },
  },
  item: {},
};