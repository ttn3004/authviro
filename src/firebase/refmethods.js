import { auth } from 'firebase/firebaseIndex';
import { db } from 'firebase/firebaseIndex';

// firebase helper methods go here...
export function getUserCollection(collection) {
  return new Promise((resolve, reject) => {
    const request = db.collection(collection);
    request.get().then(
      (doc) => {
        const newArr = [];
        doc.forEach((element) => {
          const obj = element.data();
          obj.uid = element.id;
          newArr.push(obj);
        });
        resolve(newArr);
      },
      (error) => {
        console.error(error);
        reject([]);
      }
    );
  });
}
export function getRef() {
  return new Promise((resolve, reject) => {
    setTimeout(function () {
      const request = db.collection('category');
      resolve(request.get());
    }, 20);
  });
}
export function getRef2() {
  return new Promise((resolve, reject) => {
    const request = db.collection('category');
    request
      .where('userId', '==', auth.currentUser.uid)
      .get()
      .then(
        (doc) => {
          const newArr = [];
          doc.forEach((element) => {
            newArr.push(element.data());
          });
          resolve(newArr);
        },
        (error) => {
          console.error(error);
          reject(error);
        }
      );
  });
}
