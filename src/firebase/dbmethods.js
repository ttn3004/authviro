import { auth, storageRef, storage } from 'firebase/firebaseIndex';
import { db } from 'firebase/firebaseIndex';
import { algoliaIndex } from 'algolia/algoliaIndex';
import { algolia } from 'config/appConfig';
import {
  firebaseCollection as modelCollection,
  formAddModel as modelConfig,
} from 'config/modelConfig.js';
import {
  firebaseCollection as categoryCollection,
  formAddCategory as categoryConfig,
} from 'config/categoryConfig.js';

import { readImage, isImageFile } from 'util/ImageUtil';
export const LIMIT_TO = 10000;
export const DOC_PER_PAGE = 10;

export const profileModel = {
  getInfosFromCollection: async (uid, collection) => {
    return new Promise(async (resolve, reject) => {
      try {
        let doc = await db.collection(collection).doc(uid).get();
        if (doc) resolve(doc.data());
        else resolve(null);
      } catch (error) {
        reject(error);
      }
    });
  },
};
export const dbModel = {
  save: async (name, data, modelConfig, idGen, docToUpd) => {
    return genericModel.save(name, data, modelConfig, idGen, docToUpd);
  },
  delete: async (doc) => {
    return new Promise(async (resolve, reject) => {
      await deleteGeneric(doc, modelCollection, modelConfig);
      resolve('ok');
    });
  },
  deleteFile: async (collectionName, field, file, doc) => {
    return new Promise(async (resolve, reject) => {
      await deleteFileGeneric(collectionName, field, file, doc);
      const docUpd = await db.collection(collectionName).doc(doc.uid).get();
      resolve(docUpd);
    });
  },
  getDoc: async (collectionName, docId) => {
    return new Promise(async (resolve, reject) => {
      const docRes = await getDocGeneric(collectionName, docId);
      resolve(docRes);
    });
  },
};
export const categoryModel = {
  delete: async (doc) => {
    return new Promise(async (resolve, reject) => {
      await deleteGeneric(doc, categoryCollection, categoryConfig);
      resolve('ok');
    });
  },
};
export const genericModel = {
  getDoc: async (collectionName, docId) => {
    return new Promise(async (resolve, reject) => {
      const docRes = await getDocGeneric(collectionName, docId);
      resolve(docRes);
    });
  },
  save: async (name, data, config, idGen, docToUpd) => {
    return new Promise(async (resolve, reject) => {
      console.log('save doc', data);
      const toSave = {};
      const filesToSave = {};
      /** save model firstly */
      Object.keys(data).map((key) => {
        const value = data[key];
        const type =
          key !== 'uid' && config[key] && config[key].type
            ? config[key].type
            : null;
        switch (type) {
          case 'file':
            const newObjectFile = {};
            const hosting = config[key].hosting;
            newObjectFile.host = hosting;
            newObjectFile.files = value;
            filesToSave[key] = newObjectFile;
            break;
          default:
            //type text
            toSave[key] = value;
            break;
        }
      });
      let doc = null;
      let fieldsData = {};
      let idDoc = data.uid;
      if (idDoc) {
        //case update document
        fieldsData = { ...toSave };
      } else {
        toSave.userId = auth.currentUser.uid;
        /**case force id when create */
        if (idGen) {
          toSave.uid = idGen;
          await db
            .collection(name)
            .doc(idGen)
            .set(toSave)
            .catch((error) => {
              console.error(error);
              reject(error);
            });
          idDoc = idGen;
          doc = await db.collection(name).doc(idGen).get();
        } else {
          doc = await db
            .collection(name)
            .add(toSave)
            .catch((error) => {
              console.error(error);
              reject(error);
            });
          idDoc = doc.id;
        }
      }

      const dataFieldsToUpdate = await updateFilesGeneric(idDoc, filesToSave);
      let fieldsToUpd = {};
      dataFieldsToUpdate.forEach((element) => {
        //in case to update document , get existing and merge to object
        if (docToUpd) {
          Object.keys(element).map((key) => {
            const arrFileEl = element[key];

            if (docToUpd[key]) {
              //if key exist in document existing
              const fieldsExisting = docToUpd[key];
              fieldsExisting.map((f) => {
                arrFileEl.push(f);
              });
            }
          });
        }
        fieldsToUpd = { ...element };
      });
      await updateGeneric(name, { ...fieldsData, ...fieldsToUpd }, idDoc);
      doc = await db.collection(name).doc(idDoc).get(); //refresh
      if (algolia.indexed) genericModel.saveOrUpdateIndexAlgolia(doc);
      resolve(doc);
    });
  },
  delete: async (doc, name, config) => {
    return new Promise(async (resolve, reject) => {
      await deleteGeneric(doc, name, config);
      if (algolia.indexed) genericModel.deleteIndex(doc);
      resolve('ok');
    });
  },
  saveOrUpdateIndexAlgolia: (doc) => {
    const toIndex = [];
    // Add or update new objects
    if (Array.isArray(doc)) {
      doc.forEach((element) => {
        const docIndex = element.data();
        docIndex.objectID = doc.uid ? doc.uid : doc.id;
        toIndex.push(docIndex);
      });
    } else {
      const docIndex = doc.data();
      docIndex.objectID = doc.uid ? doc.uid : doc.id;
      console.log(docIndex);
      toIndex.push(docIndex);
    }
    algoliaIndex
      .saveObjects(toIndex)
      .then(() => {
        console.log('Document imported into Algolia');
      })
      .catch((error) => {
        console.error('Error when importing document into Algolia', error);
      });
  },
  deleteIndex: (doc) => {
    // Get Algolia's objectID from the Firebase object key
    const objectID = doc.uid;
    // Remove the object from Algolia
    algoliaIndex
      .deleteObject(objectID)
      .then(() => {
        console.log('Firebase object deleted from Algolia', objectID);
      })
      .catch((error) => {
        console.error('Error when deleting contact from Algolia', error);
      });
  },
  deleteFile: async (collectionName, field, file, doc) => {
    return new Promise(async (resolve, reject) => {
      await deleteFileGeneric(collectionName, field, file, doc);
      const docUpd = await db.collection(collectionName).doc(doc.uid).get();
      resolve(docUpd);
    });
  },
};

const getDocGeneric = (collectionName, docId) => {
  return new Promise(async (resolve, reject) => {
    try {
      const doc = await db.collection(collectionName).doc(docId).get();
      resolve(doc);
    } catch (error) {
      reject(error);
    }
  });
};

const updateGeneric = (collectionName, dataFields, docId) => {
  return new Promise(async (resolve, reject) => {
    const doc = await db
      .collection(collectionName)
      .doc(docId)
      .update({ ...dataFields, dateUpdate: Date.now() })
      .catch((error) => {
        console.error(error);
        reject(error);
      });
    resolve(doc);
  });
};
const deleteFileGeneric = (collectionName, field, file, docToUpd) => {
  return new Promise(async (resolve, reject) => {
    const newArrFile = docToUpd[field].filter((e) => e.ref !== file.ref);
    const dataFields = {};
    dataFields[field] = newArrFile;
    try {
      const newDoc = await db
        .collection(collectionName)
        .doc(docToUpd.uid)
        .update({ ...dataFields, dateUpdate: Date.now() })
        .catch((error) => {
          console.error(error);
          reject(error);
        });

      const res = await deleteSingleFile(file);
      console.log('delete', res);
      resolve(newDoc);
    } catch (error) {
      reject(error);
    }
  });
};

const deleteGeneric = async (doc, collectionName, config) => {
  return new Promise(async (resolve, reject) => {
    const filesToDelete = [];
    Object.keys(doc).map((key) => {
      if (config[key] && config[key].type) {
        const value = doc[key];
        const type = config[key].type;
        switch (type) {
          case 'file':
            value.forEach((element) => {
              filesToDelete.push(element);
            });
            break;
          default:
            break;
        }
      }
    });
    //console.log('delete', filesToDelete);
    /**firstly delete all files of document */
    const res = await deleteFiles(filesToDelete);
    /**and next delete document */

    if (Array.isArray(res) || res === 'ok') {
      await db
        .collection(collectionName)
        .doc(doc.uid)
        .delete()
        .catch((error) => {
          console.error(error);
          reject(error);
        });
      //console.log('deleteGeneric', collectionName, res, doc.uid);
    } else {
      reject('error on removing files');
    }
    resolve('ok');
  });
};

/***************** STORAGE ******************/
const updateFilesGeneric = (doc, filesToSave) => {
  return new Promise(async (resolve, reject) => {
    let promises = Object.keys(filesToSave).map(async (key) => {
      const fileList = filesToSave[key].files;
      const hosting = filesToSave[key].host;
      const dataFieldsToUpdate = {};
      const resArr = await uploadFiles(doc, hosting, fileList);
      dataFieldsToUpdate[key] = resArr;
      return dataFieldsToUpdate;
    });
    const res = await Promise.all(promises);
    resolve(res);
  });
};

const uploadFiles = (doc, hosting, fileList) => {
  return new Promise(async (resolve, reject) => {
    let promises = Array.from(fileList).map(async (file) => {
      const urlToSave = `${hosting}/${doc}/${file.name}`;
      const singleUrl = await uploadSingleFile(doc, urlToSave, file);
      return singleUrl;
    });
    const arrUrl = await Promise.all(promises);
    resolve(arrUrl);
  });
};

const uploadSingleFile = (doc, urlToSave, file) => {
  return new Promise(async (resolve, reject) => {
    const image = { ref: urlToSave };
    await storageRef
      .child(urlToSave)
      .put(file)
      .catch((error) => {
        console.error(error);
        reject(error);
      });
    const url = await storageRef.child(urlToSave).getDownloadURL();
    if (isImageFile(file)) {
      console.log('readFile dimension');
      const dimension = await readImage(file);
      image.width = dimension.width;
      image.height = dimension.height;
    }

    image.url = url;
    image.name = file.name;
    resolve(image);
  });
};

const deleteFiles = (fileList) => {
  return new Promise(async (resolve, reject) => {
    if (fileList.length > 0) {
      let promises = fileList.map(async (file) => {
        const res = await deleteSingleFile(file);
        return res;
      });
      const res = await Promise.all(promises);
      //console.log('deleteFiles', fileList.length);
      resolve(res);
    } else {
      resolve('ok');
    }
  });
};

const deleteSingleFile = (file) => {
  return new Promise(async (resolve, reject) => {
    let imageRef = storage.refFromURL(file.url);
    await imageRef
      /* .child(file.ref) */
      .delete()
      .catch((error) => {
        console.error(error);
        reject(error);
      });
    resolve('ok');
  });
};
/***************** END STORAGE ******************/
export const getDocs = (
  collectionName,
  whereClauses,
  orderClauses,
  limitClauses
) => {
  return new Promise(async (resolve, reject) => {
    let query = db.collection(collectionName);
    /** whereClauses is a array of {field,operator,condition} */
    if (whereClauses && Array.isArray(whereClauses)) {
      whereClauses.forEach((e) => {
        query = query.where(e.field, e.operator, e.condition);
      });
    }
    /** orderClauses is a array of {field,operator} */
    if (orderClauses && Array.isArray(orderClauses)) {
      orderClauses.forEach((e) => {
        query = query.orderBy(e.field, e.operator);
      });
    }
    /** limitClauses is number */
    const limit = limitClauses ? limitClauses : LIMIT_TO;
    const collectRes = [];
    await query
      .limit(limit)
      .get()
      .then(
        (doc) => {
          doc.forEach((element) => {
            const obj = { uid: element.id, ...element.data() };
            collectRes.push(obj);
          });
        },
        (error) => {
          console.error(error);
        }
      );
    resolve(collectRes);
  });
};

export const paginateDocs = (
  collectionName,
  whereClauses,
  orderClauses,
  currentPage,
  itemsPerPage
) => {
  return new Promise(async (resolve, reject) => {
    let query = db.collection(collectionName);
    /** whereClauses is a array of {field,operator,condition} */
    if (whereClauses && Array.isArray(whereClauses)) {
      whereClauses.forEach((e) => {
        query = query.where(e.field, e.operator, e.condition);
      });
    }
    /** orderClauses is a array of {field,operator} */
    if (orderClauses && Array.isArray(orderClauses)) {
      orderClauses.forEach((e) => {
        query = query.orderBy(e.field, e.operator);
      });
    }
    let finalQuery = null;
    /** limitClauses is number */
    const limit = itemsPerPage ? itemsPerPage : DOC_PER_PAGE;
    const collectRes = [];
    if (currentPage === 1) {
      finalQuery = query;
    } else {
      const lastDocPosition = currentPage * limit - limit;
      const documentSnapshots = await query.get();
      if (lastDocPosition > documentSnapshots.docs.length - 1) {
        resolve([]);
      } else {
        // Get the last visible document
        let lastVisible = documentSnapshots.docs[lastDocPosition - 1];
        // Construct a new query starting at this document,
        finalQuery = query.startAfter(lastVisible);
      }
    }

    await finalQuery
      .limit(limit)
      .get()
      .then(
        (doc) => {
          doc.forEach((element) => {
            const obj = { uid: element.id, ...element.data() };
            collectRes.push(obj);
          });
        },
        (error) => {
          console.error(error);
        }
      );
    resolve(collectRes);
  });
};

export const getDocsOnSnapshot = (
  collectionName,
  whereClauses,
  orderClauses,
  limitClauses
) => {
  return new Promise(async (resolve, reject) => {
    let query = db.collection(collectionName);
    /** whereClauses is a array of {field,operator,condition} */
    if (whereClauses && Array.isArray(whereClauses)) {
      whereClauses.forEach((e) => {
        query = query.where(e.field, e.operator, e.condition);
      });
    }
    /** orderClauses is a array of {field,operator} */
    if (orderClauses && Array.isArray(orderClauses)) {
      orderClauses.forEach((e) => {
        query = query.orderBy(e.field, e.operator);
      });
    }
    /** limitClauses is number */
    const limit = limitClauses ? limitClauses : LIMIT_TO;
    const res = await query.limit(limit);
    resolve(res);
  });
};
