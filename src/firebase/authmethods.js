import { auth } from 'firebase/firebaseIndex';
import firebase from 'firebase';
import { needEmailVerification } from 'config/appConfig';
export const authMethods = {
  // firebase helper methods go here...
  signup: (email, password) => {
    return new Promise(async (resolve, reject) => {
      try {
        await auth.createUserWithEmailAndPassword(email, password);
        if (needEmailVerification) {
          const user = auth.currentUser;
          await user.sendEmailVerification();
          resolve({ type: 'info', msg: 'Email verification sended' });
        } else {
          resolve({ type: 'info', msg: 'signup ok' });
        }
      } catch (error) {
        reject({ type: 'error', msg: error.message });
      }
    });
  },
  signin: (email, password) => {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await auth.signInWithEmailAndPassword(email, password);
        if (needEmailVerification) {
          if (res.user && res.user.emailVerified) {
            resolve(res);
          } else {
            reject({ type: 'error', msg: 'Email is not verified' });
          }
        } else {
          resolve(res);
        }
      } catch (error) {
        reject({ type: 'error', msg: error.message });
      }
    });
  },
  resetPwdByEmail: (email) => {
    return new Promise(async (resolve, reject) => {
      try {
        await auth.sendPasswordResetEmail(email);
        resolve({ type: 'info', msg: 'Email sended for reset password' });
      } catch (error) {
        reject({ type: 'error', msg: error.message });
      }
    });
  },
  signinWithGoogle: () => {
    const provider = new firebase.auth.GoogleAuthProvider();
    return new Promise(async (resolve, reject) => {
      try {
        await auth.signInWithPopup(provider);
        resolve({ type: 'info', msg: 'Signin with google ok' });
      } catch (error) {
        reject({ type: 'error', msg: error.message });
      }
    });
  },
  signinWithPhone: (phone, appVerifier) => {
    return new Promise(async (resolve, reject) => {
      try {
        const confirmResult = await auth.signInWithPhoneNumber(
          phone,
          appVerifier
        );
        resolve(confirmResult);
      } catch (error) {
        reject({ type: 'error', msg: error.message });
      }
    });
  },
  signout: () => {
    return new Promise(async (resolve, reject) => {
      try {
        await auth.signOut();
        resolve({ type: 'info', msg: 'signout ok' });
      } catch (error) {
        console.error(error.message);
        reject({ type: 'error', msg: error.message });
      }
    });
  },
  currentUser: () => {
    return auth.currentUser;
  },
};
