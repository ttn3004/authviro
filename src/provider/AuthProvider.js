import React, { useState, useEffect } from 'react';
import { authMethods } from 'firebase/authmethods';
import { profileModel } from 'firebase/dbmethods';
import { auth } from 'firebase/firebaseIndex';
import { useHistory } from 'react-router-dom';
import { profileConfig, needEmailVerification } from 'config/appConfig';
export const FirebaseAuth = React.createContext();
const AuthProvider = (props) => {
  const [response, setResponse] = useState([]);
  const [user, setuser] = useState(authMethods.currentUser());
  const [profile, setProfile] = useState(null);
  const [fromLoc, setfromLoc] = useState(null);
  const [loading, setLoading] = useState(true);
  const [numberOfRedirect, setNumberOfRedirect] = useState(0);
  const handleSignup = async (email, password) => {
    setLoading(true);
    try {
      const res = await authMethods.signup(email, password);
      setResponse([res]);
    } catch (error) {
      setResponse([error]);
    }
  };
  const handleSignin = async (email, password) => {
    setLoading(true);
    try {
      const res = await authMethods.signin(email, password);
      if (res.user) {
        setuser(res.user);
      } else {
        setResponse([res]);
      }
    } catch (error) {
      console.error('AuthProvider', error);
      setResponse([error]);
    }
  };
  const handleResetPwd = async (email) => {
    setLoading(true);
    try {
      const res = await authMethods.resetPwdByEmail(email);
      setResponse([res]);
    } catch (error) {
      console.error(error);
      setResponse([error]);
    }
  };
  const handleSigninWithGoogle = async () => {
    try {
      const res = await authMethods.signinWithGoogle();
      setResponse([res]);
    } catch (error) {
      setResponse([error]);
    }
    return;
  };
  /**Phone authentification */
  const handleSendCode = async (phone, appVerifier) => {
    try {
      const res = await authMethods.signinWithPhone(phone, appVerifier);
      return res;
    } catch (error) {
      setResponse([error]);
    }
  };
  const handleVerifyCode = async (confirmResult, verificationCode) => {
    try {
      const user = await confirmResult.confirm(verificationCode);
      setuser(user);
    } catch (error) {
      console.error(error);
      setResponse([{ type: 'error', msg: error.message }]);
    }
  };
  /** */
  const handleSignout = async () => {
    try {
      await authMethods.signout(setuser);
    } catch (error) {
      console.error(error);
    }
  };
  const history = useHistory();
  /**LISENER FOR USER CHANGES */
  useEffect(() => {
    auth.onAuthStateChanged(async function (user) {
      if (user) {
        let canContinue = true;
        if (needEmailVerification) {
          if (!user.emailVerified) canContinue = false;
        } else {
          setuser(user);
        }
        /**
      get profile user if needed
       */
        if (canContinue) {
          const profileDoc = await profileModel.getInfosFromCollection(
            user.uid,
            profileConfig.profileCollection
          );
          if (profileDoc) setProfile(profileDoc);
        }

        if (fromLoc && user && numberOfRedirect < 4) {
          const newNb = numberOfRedirect + 1;
          setNumberOfRedirect(newNb);
          history.push(fromLoc);
        } else {
          setNumberOfRedirect(0);
        }
      } else {
        setuser(null);
      }
      setLoading(false);
      //console.log('AuthProvider', user.uid);
    });
  }, [fromLoc]);
  return (
    <FirebaseAuth.Provider
      value={{
        handleSignup,
        handleSignout,
        handleSignin,
        handleSigninWithGoogle,
        response,
        setResponse,
        user,
        loading,
        setLoading,
        setfromLoc,
        profile,
        setProfile,
        handleResetPwd,
        handleSendCode,
        handleVerifyCode,
      }}
    >
      {props.children}
    </FirebaseAuth.Provider>
  );
};
export default AuthProvider;
