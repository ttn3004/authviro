import React, { useState, useEffect, useContext } from 'react';
import {
  LIMIT_TO,
  DOC_PER_PAGE,
  getDocs,
  getDocsOnSnapshot,
} from 'firebase/dbmethods';
import { db } from 'firebase/firebaseIndex';
import { FirebaseAuth } from 'provider/AuthProvider';

export const GenericCtx = React.createContext();
const GenericDbProvider = ({
  collectionName,
  filterCollection,
  orderCollection,
  paging,
  nbItemsPerPage,
  ...props
}) => {
  const [items, setitems] = useState([]);
  const [filter, setFilter] = useState([]);
  const [orderFilter, setOrderFilter] = useState([]);
  const [errors, seterrors] = useState(null);
  const [queryChange, setQueryChange] = useState(null);
  /**paging */
  const [currentPage, setCurrentPage] = useState(paging ? 1 : null);
  const [itemsPerPage, setItemsPerPage] = useState(nbItemsPerPage);
  const [docsSize, setDocsSize] = useState(null);
  const [totalPage, setTotalPage] = useState(null);
  const [isPaging, setPaging] = useState(paging ? true : null);
  const [lastDoc, setLastDoc] = useState(false);
  const [endOfPaging, setEndOfPaging] = useState(false);

  const { loading, setLoading } = useContext(FirebaseAuth);

  useEffect(() => {
    setLoading(true);
    console.log(
      '********************* queryChange*********************',
      /*       queryChange,
      filter, */
      endOfPaging
    );
    if (isPaging && endOfPaging) {
      setLoading(false);
      return;
    }
    let query = db.collection(collectionName);
    if (filter && filter.length > 0) {
      filter.forEach((e) => {
        switch (e.type) {
          default:
            if (e.field && e.operator && e.value) {
              query = query.where(e.field, e.operator, e.value);
            }
            break;
        }
      });
    }
    const orderFilt =
      orderFilter && orderFilter.length > 0 ? orderFilter : constructOrder();
    if (orderFilt && orderFilt.length > 0) {
      orderFilt.forEach((e) => {
        /* Order by clause cannot contain a field with an equality filter name */
        const isFilterContain = filter.filter(
          (obj) =>
            obj.field && obj.operator && obj.value && obj.field === e.field
        );
        if (e.field && e.operator && isFilterContain.length === 0) {
          query = query.orderBy(e.field, e.operator);
        }
      });
    }
    /** limitClauses is number */
    /**construct query */
    let finalQuery = null;
    /** limitClauses is number */
    const limit = isPaging
      ? itemsPerPage
        ? itemsPerPage
        : DOC_PER_PAGE
      : LIMIT_TO;
    if (isPaging && lastDoc) {
      finalQuery = query.startAfter(lastDoc);
      /* console.log('lastDoc', lastDoc); */
    } else {
      finalQuery = query;
    }
    const unsubscribe = finalQuery.limit(limit).onSnapshot(
      (doc) => {
        const collectRes = [...items];
        let ord = 0;
        doc.forEach(async (element) => {
          ord++;
          const obj = { uid: element.id, ...element.data() };
          collectRes.push(obj);
          if (isPaging && ord === itemsPerPage) {
            const lastVisited = await db
              .collection(collectionName)
              .doc(element.id)
              .get();
            setLastDoc(lastVisited);
            setEndOfPaging(false);
          }
        });
        if (isPaging && ord < itemsPerPage) {
          setEndOfPaging(true);
        }
        setitems(collectRes);
        setLoading(false);
      },
      (error) => {
        console.error(error);
        seterrors(error);
        setLoading(false);
      }
    );

    return () => unsubscribe();
  }, [queryChange]);

  useEffect(() => {
    //console.log('GenericDbProvider-useEffect filter');
    constructFilter();
  }, [filterCollection]);
  const constructFilter = async () => {
    if (filterCollection && Object.keys(filterCollection).length > 0) {
      let promises = Object.keys(filterCollection).map(async (key) => {
        const objFilter = filterCollection[key];
        objFilter.key = key;
        objFilter.field = key;
        const type = objFilter.type;
        switch (type) {
          case 'text':
            break;
          case 'select':
            const nameFilterCollection = objFilter.collection;
            const docs = await getDocs(nameFilterCollection);
            objFilter.optionValues = docs;
            break;
          default:
            break;
        }
        return objFilter;
      });
      const res = await Promise.all(promises);
      setFilter(res);
    }
  };
  useEffect(() => {
    //console.log('GenericDbProvider-useEffect order', orderCollection);
    constructOrder();
  }, [orderCollection]);
  /** orderClauses is a array of {field,operator} */
  const constructOrder = () => {
    if (orderCollection && Object.keys(orderCollection).length > 0) {
      const res = [];
      Object.keys(orderCollection).map((key) => {
        const objOrd = {};
        const type = orderCollection[key].type;
        const label = orderCollection[key].label;
        objOrd.field = key;
        objOrd.type = type;
        objOrd.label = label;
        switch (type) {
          case 'text':
            objOrd.operator = orderCollection[key].operator;
            break;
          default:
            break;
        }
        res.push(objOrd);
      });
      setOrderFilter(res);
      return res;
    }
  };

  const handleResetPaging = async () => {
   // console.log('handleResetPaging');
    setitems([]);
    setLastDoc(null);
    setCurrentPage(1);
    setEndOfPaging(false);
    setQueryChange('resetPaging' + Date.now());
  };
  const handleResetQuery = async () => {
    setitems([]);
    await constructFilter();
    constructOrder();
    if (isPaging) {
      handleResetPaging();
    } else {
      setQueryChange('reset' + Date.now());
    }
  };

  return (
    <GenericCtx.Provider
      value={{
        items,
        filter,
        setFilter,
        orderFilter,
        setOrderFilter,
        currentPage,
        totalPage,
        itemsPerPage,
        setCurrentPage,
        docsSize,
        setLastDoc,
        endOfPaging,
        errors,
        setQueryChange,
        handleResetQuery,
        handleResetPaging,
      }}
    >
      {props.children}
    </GenericCtx.Provider>
  );
};
export default GenericDbProvider;
